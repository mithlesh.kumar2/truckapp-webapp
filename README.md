This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

# ENV.LOCAL
    NEXT_PUBLIC_ENVIRONMENT_MODE = dev | backend

# New Updates 

1. Input validation

    Add Payment Made , Add Payment Received,  create trip , edit trip , add truck, vechicle Registration Number

2. Disable

    disable Add Payment Made , Add Payment received buttons when balance will 0


3. Remove

    remove Mark settled button when balance will 0

    remove row where amount is 0 from "Payments Made"

4. Add

    "Add Billing Deatils" section inside edit trip page

    "edit" and "delete" in each transaction of "Payments Received" and "Payments Made" 

5. Rename

    rename the heading in edit trip page from 'Trip' to 'Edit Trip'

6. Close

    edit trip 
    
        close datalist when user click on any selected truck in "truck number input"






# nextjs version = "10.2.3"
# react = 17.0.2
# react-dom: 17.0.2

const path = require('path')


module.exports = ({
    webpack(config) {
     
      config.module.rules.push({
        test: /\.svg$/,
        issuer: {
          test: /\.(js|ts)x?$/,
        },
        use: ['@svgr/webpack']
      });
      return config;
    },
 
    sassOptions: {
      includePaths: [path.join(__dirname, 'styles')],
    },

    
});
