import moment from 'moment';
import { useRouter } from 'next/router'
import { useEffect, useState , useContext } from 'react';
import { getTripDetails , getShortLivedUrl} from '../../Services/TripDataServices'
import tripStyles from '../../styles/TripsData.module.scss'
import { LocalShippingOutlined } from '@material-ui/icons'
import { Button, Fab, Toolbar, Typography } from '@material-ui/core'
import INRIcon from '../../Components/Home/svg/InrIcon.svg'
import Download from '../../Components/Home/svg/Download.svg'
import { GlobalLoadingContext } from '../../Context/GlobalLoadingContext'


const podDetails = () => {
    const router = useRouter();
    const trip_id = router.query.trip_id
    const [tripData, setTripData] = useState({})
    const { setGlobalLoading } = useContext(GlobalLoadingContext)

    let getTrip = async () => {
        setGlobalLoading(true)
        try {
            const trip = await getTripDetails(trip_id)
            setGlobalLoading(false)
            if(trip){
                setTripData(trip)
                if(trip.pod_s3_key){
                    const image = await getShortLivedUrl(trip.pod_s3_key,'pod')
                    if(image && image.success){
                        setTripData({...trip,image:image.link})
                    }
                }
            }else{
                setGlobalLoading(false)
            }
        } catch (e) {
            setGlobalLoading(false)
        }
    }
    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }
    const download = () => {
        if(tripData.image){
            var element = document.createElement("a");
            var file = new Blob(
            [
                tripData.image
            ],
            { type: "image/*" }
            );
            element.href = URL.createObjectURL(file);
            element.download = "image.jpg";
            element.click();
        }
      };

    useEffect(() => {
        getTrip()
    }, [])

    return <>
        <table className={`w-100 rounded-3 position-relative mt-4 ${tripStyles['table']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
            <thead>
                <tr>
                    <th style={{ width: "1%" }}></th>
                    <th>Started Date</th>
                    <th>Driver Name</th>
                    <th>Number</th>
                    <th>Truck No.</th>
                    <th>Route</th>
                    <th>Freight Amount</th>
                </tr>
            </thead>
            <tbody>
                {tripData &&
                    <tr style={{ cursor: "pointer" }} >
                        <td style={{ width: "1%" }}><Fab className={tripStyles[tripData.status]} ><LocalShippingOutlined className={tripStyles[tripData.status]} /></Fab></td>
                        <td>{getDate(tripData.trip_start_date)}</td>
                        <td>{tripData?.driver_name || ""}</td>
                        <td>{tripData.driver_phone}</td>
                        <td>{tripData.truck_number}</td>
                        <td className="d-flex justify-content-center align-items-center">
                            <div className="d-flex py-1 flex-column justify-content-between align-items-start m-auto">
                                <div className="d-flex align-items-center justify-content-start">
                                    <span className={tripStyles['dot']} style={{ backgroundColor: "rgba(45, 188, 83, 1)" }}></span>
                                    <span className="mx-1">{tripData.origin_city}</span>
                                </div>
                                <span className={tripStyles["vertical-line"]}></span>
                                <div className="d-flex align-items-center justify-content-start">
                                    <span className={tripStyles["dot"]} style={{ backgroundColor: "rgba(231, 104, 50, 1)" }}></span>
                                    <span className="mx-1">{tripData.destination_city}</span>
                                </div>
                            </div>
                        </td>
                        <td><INRIcon className="inr-icon" /> {tripData.freight_amount}</td>
                    </tr>
                }
            </tbody>
        </table>
        <Toolbar className="py-4">
            <Typography style={{ flex: 1 }}>
                
                <div className='d-flex justify-content-center'>
                    {
                        tripData.image && 
                        <img
                            src={tripData.image}
                            alt="Trip Image"
                            loading="lazy"
                            width={350}
                        />
                    }
                </div>
                <br />
                <div className="pt-4 d-flex justify-content-center">
                    <Button variant="contained" color="primary" onClick={_=>router.push('/settings/podTracker')} style={{ width: '50%' }} size="large">Close</Button>
                </div>
            </Typography>
            { tripData.image && <>
                {/* <div className='align-self-start m-2 text-center text-secondary' style={{cursor:'pointer'}}>
                    <Share title="share" />
                    <br />
                    <div style={{fontSize : 12}}>SHARE</div>
                </div> */}
                <div className='align-self-start m-4 text-center text-secondary' style={{cursor:'pointer'}}>
                    <a href={tripData.image} rel="noopener noreferrer" target="_blank" downlaod onClick={download} >
                        <Download title="download" />
                        <br />
                        <div style={{fontSize : 12}}>DOWNLOAD</div>
                    </a>
                </div>
              </>
            }
        </Toolbar>
        
    </>
}

export default podDetails
