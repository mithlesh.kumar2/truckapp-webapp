import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from "react-i18next";
import { Button, ButtonGroup, TextField, Avatar } from '@material-ui/core'
import { LocalShippingOutlined } from '@material-ui/icons'
import tripStyles from './../../../styles/TripsData.module.scss'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { getAllTrips } from '../../../Services/driverKhata'
import moment from 'moment'
import { blue, green } from '@material-ui/core/colors'
import { DatePicker } from '@material-ui/pickers'
import styles from './../../../styles/CustomerData.module.scss'
import { get_transactions } from '../../../Services/driverKhata'
import AddPaymentMadeModal from './../../../Components/Home/Trip/TripsData/TripSummary/AddPaymentMadeModal'

function ExpancesStatusList() {
    const { t } = useTranslation()
    const router = useRouter();
    const trip_id = router.query.trip_id
    const [activeBtn,setActiveBtn] = useState(1)
    const [openPaymentMadeModal ,setOpenPaymentMadeModal] = useState(false)

    let handleGetTransactions = async () => {
        console.log('trip_id = ',trip_id)
        if(trip_id){
            try {
                const list = await get_transactions({
                    type: "expense",
                    trip_id:trip_id// "linkedin_trip_id = ap-south-1:6376c2e2-a84a-4cd2-b014-8ecf0ef46c2f" 
                })
                console.log('list = ', list)
            } catch (e) {
                console.log('e = ', e)
            }
        }
    }

    let handleOpenPaymentMadeModal = (s=false)=>{
        setOpenPaymentMadeModal(s)
    }

    useEffect(()=>{
        handleGetTransactions()
    },[])

    return (
        <>
        <div className="px-4 py-4" style={{background:'white'}}>
           <div className="row">
               <div className='col'>
                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                            <Button onClick={e=>setActiveBtn(1)} {...(activeBtn===1 ? {variant:"contained",color:"primary"} : {})}>Pending</Button>
                            <Button onClick={e=>setActiveBtn(2)} {...(activeBtn===2 ? {variant:"contained",color:"primary"} : {})}>Aproved</Button>
                            <Button onClick={e=>setActiveBtn(3)} {...(activeBtn===3 ? {variant:"contained",color:"primary"} : {})}>Reject</Button>
                        </ButtonGroup>
                </div>
                <div className="col-3" style={{textAlign:'right'}}>
                    <Button variant="contained" className="bg-primary text-light" onClick={_=>handleOpenPaymentMadeModal(true)}>
                        <svg className="mx-2" width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="9.5" cy="9.5" r="9.5" fill="white"/>
                            <line x1="9.26855" y1="4.38281" x2="9.26855" y2="14.6136" stroke="#5E95F7"/>
                            <line x1="14.6147" y1="9.26562" x2="4.38398" y2="9.26562" stroke="#5E95F7"/>
                        </svg>
                        Add Expense
                    </Button>
                </div>

           </div>

        </div>

        <div className="px-4 pt-4" style={{background:'white'}}>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>

                <div className='row mt-3'>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{'Fule Expense'.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                Fule Expense<br />
                                <small className="text-secondary">5 Dec 2021 8:32pm</small>
                            </div>

                        </div>
                        <div className='col-4' style={{textAlign:'right'}}>₹ 50,000</div>

                </div>
        </div>

        {openPaymentMadeModal && 
            <AddPaymentMadeModal 
                // fill={isOpenEditModal?isOpenEditModal:{}}
                // UpdateTripDetails={TripDetails} 
                // tripDetails={{
                //      ...tripDetails,
                //      trip_id: tripId
                //     }}
                ClosePaymentMadeModal={_=>{
                    handleOpenPaymentMadeModal(false)
                    console.log('ClosePaymentMadeModal')
                }}
            />}

        </>
    )
}

export default ExpancesStatusList