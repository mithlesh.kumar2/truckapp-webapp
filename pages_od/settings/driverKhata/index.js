import Link from 'next/link'
import { Box, Button, Typography } from '@material-ui/core';
import DriverKhata from './svg/driverKhata.svg'
import ExpensesList from './svg/expensesList.svg'
import SettleBalance from './svg/settleBalance.svg'
import CheckHistory from './svg/checkHistory.svg'


const driverKhata = () => {
   return <Box style={{justifyContent:'center',textAlign:'center'}}>
        <Box >
            <DriverKhata />
            <Typography varient="h7">Driver Khata</Typography>
            <Typography varient="text" className="text-secondary">Your Driver khata at One Place. Add Expenses, Settle Balance & Check History.</Typography>
        </Box>
        <div style={{marginTop:10}} className="row">
                <div className='col'>
                    <ExpensesList />
                    <p>Expenses List</p>
                </div>
                <div className='col'>
                    <SettleBalance />
                    <p>Settle Balance</p>
                </div>
                <div className='col'>
                    <CheckHistory />
                    <p>Check History</p>
                </div>
        </div>
        <Link href='/settings/driverKhata/driverList'>
            <Button variant="contained" color="primary" className="mt-5" size='medium'>CONTINUE</Button>
        </Link>
   </Box>
}

export default driverKhata
