import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from "react-i18next";
import { Button, Fab, TextField, Typography } from '@material-ui/core'
import { Delete, LocalShippingOutlined } from '@material-ui/icons'
import tripStyles from './../../../styles/TripsData.module.scss'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { getAllTrips , UpsertDriverAdvance } from '../../../Services/driverKhata'
import moment from 'moment'
import { blue, green, grey } from '@material-ui/core/colors'
import { DatePicker } from '@material-ui/pickers'
import Link from 'next/link';


function driverTripList() {
    const { t } = useTranslation()
    const router = useRouter();
    const trip_id = router.query.id //this is a driver id
    const [tripData, setTripData] = useState()
    const [date, setDATE] = useState("")

    const [paymentDetails,setPaymentDetails] = useState({
        trip_id : trip_id,
        amount : null,
        reason : '',
        date : null
    })

    let addPaymentHandler = (payload)=>{
        
        setPaymentDetails({...paymentDetails,...payload})

    }

    let AllTrips = async () => {
        console.log('trip_id = ',trip_id)
        if(trip_id){
            try {
                const list = await getAllTrips({
                    driver_uid: trip_id //"ap-south-1:ca5f55d5-cd05-4047-a73a-8ff724080722"
                })
                setTripData(list)
                console.log('list = ', list)
            } catch (e) {
                console.log('e = ', e)
            }
        }

    }

    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }

    let handleAddPayment = async ()=>{
        let payloads = {
            trip_id : paymentDetails.trip_id,
            amount : Number(paymentDetails.amount),
            reason : paymentDetails.reason || 'Payment - ' + (tripData ? (tripData.trips || []).length || 1 : 1),
            date : (new Date(moment(paymentDetails.date).format("YYYY-MM-DD")).getTime()) / 1000
        }
        
        try{
            const result = await UpsertDriverAdvance(payloads)
            console.log('payloads_add = ',payloads)
            if(result && result.success){
                AllTrips()
            }
            console.log('response_add = ',result)
        }catch(e){
            console.log('error = ',e)
        }

    }

    let handleDeletePayment = async ({ id , amount, date,reason})=>{
        let payloads = {
            trip_id : "trip16310822004995127-453-ap-south-1:9d440824-f443-4091-8d01-fd85c912ab50",
            amount : 0,
            id : id,
            reason : 'delte',
            // reason : reason,
            // is_settled : false,
            date : (new Date(moment(Date.now()).format("YYYY-MM-DD")).getTime()) / 1000
            

        }
        
        try{
            const result = await UpsertDriverAdvance(payloads)
           
            console.log('response_delete = ',result)
        }catch(e){
            console.log('error = ',e)
        }
    }

    let handleSettement = async ()=>{
        let payloads = {
            trip_id : trip_id,
            amount : 10,
            reason : 'setelemnt reason',
            is_settled : true,
            date : (new Date(moment(Date.now()).format("YYYY-MM-DD")).getTime()) / 1000

        }
        
        try{
            const result = await UpsertDriverAdvance(payloads)
           
            console.log('response_handleSettement = ',result)
        }catch(e){
            console.log('error = ',e)
        }
    }

    useEffect(() => {
        AllTrips()
        setTimeout(_=>{
            handleSettement()
        },3000)
    }, [])

    return (
        <>
            <table className={`w-100 rounded-3 position-relative mt-4 ${tripStyles['table']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
                <thead>
                    <tr>
                        <th style={{ width: "1%" }}></th>
                        <th>Part Name</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Truck Number</th>
                        <th>Status</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>

                    {tripData &&
                        (tripData.trips || []).map(({
                            trip_id,customer_name, customer_uid, destination_city, driver_balance,
                            origin_city, status, trip_start_date, truck_number, driver_phone, driver_name,
                            truck_id , driver_advance
                        }, i) => {
                            let total = '' , advances = []
                            if(driver_advance && 'object'===typeof driver_advance && !Array.isArray(driver_advance)){
                                total = driver_advance.total
                                advances = driver_advance.advances
                            }
                            const numType = n=>-1 >= n ? 'neg' : 'pos'
                            let blType = numType(driver_balance)

                        return <React.Fragment key={i + trip_id}>
                                <tr style={{ cursor: "pointer" }}>
                                    <td style={{ width: "1%" }}><Fab className={tripStyles[status]} ><LocalShippingOutlined className={tripStyles[status]} /></Fab></td>
                                    <td>
                                        {customer_name}<br />
                                        {driver_phone}
                                    </td>
                                    <td>
                                        {origin_city}<br />
                                        {getDate(trip_start_date)}
                                    </td>
                                    <td>{destination_city}</td>
                                    <td>{truck_id}</td>
                                    <td style={{ textTransform: 'capitalize' }}><span className={tripStyles[status]}>{
                                        status.replace('_', ' ')
                                    }</span></td>
                                    <td>
                                        <div><small>{blType==='neg' ? 'To Pay' : 'To Receive'}</small></div>
                                        <span style={{fontWeight:'bold',color:blType==='neg' ? '#F53636' : '#2dbc53'}}>
                                            <INRIcon className="inr-icon" /> {Math.abs(driver_balance)}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colSpan={7}>
                                        <div className="row mt-4">
                                            <div className="col" style={{ textAlign: 'left', fontSize: '.9rem' }}>Payment Details</div>
                                            <div className="col-4" onClick={_=>addPaymentHandler({trip_id : trip_id})} style={{ textAlign: 'right', color: '#312968', cursor: 'pointer' }}>+ Add payment </div>

                                            <div className="container px-4" style={{display : ((paymentDetails.trip_id===trip_id) || Array.isArray(advances) && advances.length)? 'block' : 'none'}} >
                                                {(Array.isArray(advances) && advances.length) && advances.map(({ id , amount, date,reason},i)=>
                                                    <div className="row rounded-3 p-1 mt-3" style={{background:blue[50]}} key={i+id}>
                                                        <div className='col align-self-center' style={{textAlign:'left'}}>
                                                            {reason}<br/>
                                                            <small className='text-default'>Date: {getDate(date)}</small>
                                                        </div>
                                                        <div className='col-4 align-self-center' style={{textAlign:'right'}}>
                                                            <INRIcon className="inr-icon" /> {Math.abs(amount)}
                                                            <span style={{cursor:'pointer'}} onClick={_=>{
                                                                handleDeletePayment({ id , amount, date,reason})
                                                                console.log('click on delete = ',id)
                                                            }} className='mx-3'><Delete htmlColor={grey[500]} fontSize="small" /></span>
                                                        </div>
                                                    </div>
                                                )

                                                }
                                                { paymentDetails.trip_id===trip_id && 
                                                    <>
                                                        {
                                                   
                                                            <div className='row my-3 border px-3 py-2'>
                                                                <div className="col">
                                                                    <TextField value={paymentDetails.amount} onChange={e=>addPaymentHandler({amount:e.target.value})} label={t('Enter Amount')} style={{ minWidth: '100%' }} />
                                                                </div>
                                                                <div className="col">
                                                                    <TextField value={paymentDetails.reason} onChange={e=>addPaymentHandler({reason:e.target.value})} label={t('Reason')} style={{ minWidth: '100%' }} />
                                                                </div>
                                                                <div className="col">
                                                                    <DatePicker
                                                                        label={t('dd-mm-yyyy')}
                                                                        value={ paymentDetails.date || null }
                                                                        onChange={e=>addPaymentHandler({date : e})}
                                                                        format="DD-MM-YYYY"
                                                                        autoOk
                                                                    />
                                                                </div>
                                                                <div className="col-5 align-self-center" style={{ textAlign: 'right' }}>
                                                                    <span style={{cursor:'pointer'}} onClick={_=>{
                                                                        console.log('clear = ')
                                                                        addPaymentHandler({
                                                                            trip_id : '',
                                                                            date : null,
                                                                            amount : null,
                                                                            reason : ''
                                                                        })
                                                                    }}><Button variant="contained" className='bg-default mx-3' size="small">Cancle</Button></span>
                                                                    <span style={{cursor:'pointer'}} onClick={_=>{
                                                                        console.log('click on save')
                                                                        handleAddPayment()
                                                                    }}><Button variant="contained" className='bg-primay mx-3' size="small">Save</Button></span>
                                                                </div>
                                                            </div>
                                                    
                                                        }
                                               
                                                    </>
                                                }
                                            </div>

                                            <div className="w-100 mt-3"></div>
                                            <div className="col" style={{ textAlign: 'left' }}>Total Payments </div>
                                            <div className="col-4 font-weight-bold mx-5" style={{ textAlign: 'right' , fontWeight:'bolder'}}><INRIcon className="inr-icon" /> {total}</div>
                                        </div>
                                        <div className='row pt-5'>
                                            <div className="col"></div>
                                            <div className="col-6" style={{ textAlign: 'right' }}>
                                                <Button variant="outlined" style={{cursor:'pointer' ,width: 'fit-content', background: blue[400] }}  className="border-0 text-light" size="large">Expenses List</Button>
                                                <Button variant="outlined" style={{cursor:'pointer' ,width: 'fit-content', background: green[400] }} className="border-0 text-light mx-3" size="large">Settlement</Button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </React.Fragment>
                        })

                    }
                </tbody>
            </table>

            <div className="pt-5 text-center">
                <Link href="/settings/driverKhata/driverList">
                    <Button className="bg-primary px-5 text-light" size="large">CANCLE</Button>
                </Link>
            </div>
        </>
    )
}

export default driverTripList