import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, TextField, Avatar } from '@material-ui/core';
import styles from './../../../styles/CustomerData.module.scss'
import Link from 'next/link'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { PulseLoader } from 'react-spinners'
import { getHistory } from '../../../Services/driverKhata'
import Activateupdown from '../../../Components/Home/svg/Activateupdown.svg'
import ArrowRight from './svg/arrowRight.svg'
import { Delete } from '@material-ui/icons'
import Eye from '../../../Components/Home/svg/Eye.svg'
import ConfirmDelete from '../../../Components/ConfirmDelete';


const settleDriver = () => {
    const { t } = useTranslation()
    const [openConfirmDialog,setOpenConfirmDialog] = useState(false)

    const handleDelete = async ()=>{
        setOpenConfirmDialog(false)
    }



    return (
        <>
            <div className="mx-4 px-4 py-3" style={{ background: 'white' }}>

                <div>
                    <div className="font-weight-bold h5">Settle Driver</div>
                    <div className='row text-secondary'>
                        <div className='col'>Adavnce Paymennt</div>
                        <div className='col-4' style={{ textAlign: 'right' }}>₹ 9,000/-</div>
                        <div className='w-100'></div>
                        <div className='col'>Extra Advance </div>
                        <div className='col-4' style={{ textAlign: 'right' }}>₹ 9,000/-</div>
                    </div>
                    <hr style={{ borderTop: '1px dashed white' }} />
                    <div className='row'>
                        <div className='col'>Total  Advance </div>
                        <div className='col-4 ' style={{ textAlign: 'right' }}>₹ 18,000/-</div>
                    </div>
                </div>

                <div className="pt-5">
                    <div className="font-weight-bold h5">ALL Approved Expenses</div>
                    <div className='row text-secondary'>
                        <div className='col'>Brokerage </div>
                        <div className='col-4 d-flex justify-content-end' style={{ textAlign: 'right' }}>
                            ₹ 500
                            <Link href="#">
                                <div className='px-3' style={{ cursor: 'pointer' }}>
                                    <Eye className='mx-1' />
                                    <span style={{ textDecoration: 'underline' }}>View Details</span>
                                </div>
                            </Link>
                            <Delete style={{cursor:'pointer'}} className='px-1 mx-1' onClick={_=>setOpenConfirmDialog(true)} />
                        </div>
                        <div className='w-100'></div>
                        <div className='col'>Fuel Expense </div>
                        <div className='col-4 d-flex justify-content-end' style={{ textAlign: 'right' }}>
                            ₹ 7500
                            <Link href="#">
                                <div className='px-3' style={{ cursor: 'pointer' }}>
                                    <Eye className='mx-1' />
                                    <span style={{ textDecoration: 'underline' }}>View Details</span>
                                </div>
                            </Link>
                            <Delete style={{cursor:'pointer'}} className='px-1 mx-1' onClick={_=>setOpenConfirmDialog(true)} />
                        </div>                        <div className='w-100'></div>
                        <div className='col'>Police Expense </div>
                        <div className='col-4 d-flex justify-content-end' style={{ textAlign: 'right' }}>
                            ₹ 100
                            <Link href="#">
                                <div className='px-3' style={{ cursor: 'pointer' }}>
                                    <Eye className='mx-1' />
                                    <span style={{ textDecoration: 'underline' }}>View Details</span>
                                </div>
                            </Link>
                            <Delete style={{cursor:'pointer'}} className='px-1 mx-1' onClick={_=>setOpenConfirmDialog(true)} />
                        </div>                    </div>
                    <div className='row border mt-4 py-3 alert-link' style={{ color: '#080B40' }}>
                        <div className='col'>
                            Balance Amount<br />
                            <small>( Total Advance - Approved Expenses )</small>
                        </div>
                        <div className='col-4 align-self-center' style={{ textAlign: 'right' }}>₹ 10,400/-</div>
                    </div>
                </div>

                <div className='row mt-4 py-3 alert-link' style={{ color: '#312968' }}>
                    <div className='col'>Custom Settlement</div>
                    <div className='col-3 align-self-center' style={{ textAlign: 'right' }}>
                        <TextField type="number" label={t('Enter Amount')} style={{ minWidth: '90%' }} />
                    </div>
                </div>

                <div className="text-center mt-5">
                    <Button className="bg-primary text-light px-4" size="medium">Settle Balance</Button>
                </div>

                {
                    openConfirmDialog && 
                    <ConfirmDelete open={openConfirmDialog} close={() => setOpenConfirmDialog(false)} action={handleDelete}
                        message1='Delete the Entry ?'
                        message2='If you delete this Entry will be gone forever . Are you sure want to proceed? '
                    />
                }

            </div>
        </>
    )
}

export default settleDriver
