import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, TextField, Avatar } from '@material-ui/core';
import styles from './../../../styles/CustomerData.module.scss'
import Link from 'next/link'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { PulseLoader } from 'react-spinners'
import { getAllTrips , get_transactions } from '../../../Services/driverKhata'
import Activateupdown from '../../../Components/Home/svg/Activateupdown.svg'
import ArrowRight from './svg/arrowRight.svg'
import moment from 'moment'


const History = () => {
    const { t } = useTranslation()
    const [settledHistory, setSettledHistory] = useState({
        from: null,
        trips: [],
        type: ''//search
    })
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('')

    // settlement history
        // "type": "driver_settled",
    
    // driver settlement

    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }


    let driverList = async () => {
        try {
            const temp = {
                // "type": "driver_settled",
                driver_uid : 'ap-south-1:dce7d742-7668-492d-9f03-f50769c84428',
                "trip_id": "trip16310385182811556-632-ap-south-1:9d440824-f443-4091-8d01-fd85c912ab50",
                type : 'expense'
            }
            // if(totalDrivers.type==='search'){
            //     temp.name = search
            // }
            // if(totalDrivers.token.length){
            //     temp.token = totalDrivers.token
            // }
            const list = await getAllTrips({
                type: "driver_settled"
            })
            setSettledHistory({...settledHistory,...list})
            // await 
            console.log('response    = ', list)

            // setTotalDrivers({
            //     summary : list.summary,
            //     token : list.token ? JSON.parse(list.token) : [],
            //     type : totalDrivers.type,
            // })
        } catch (e) {
            console.log('e = ', e)
        }
    }

    let handleSearch = (e) => {
        setSearch(e.target.value)
        setTotalDrivers({ ...totalDrivers, type: 'search' })
    }

    useEffect(() => {
        driverList()

    }, [search])

    return (
        <>
            <div className="row justify-content-between align-item-end">
                <div className="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-5">
                    <TextField onChange={handleSearch} value={search} label={t('Search Driver Name')} style={{ minWidth: '90%' }} />
                </div>

            </div>

            <div className="px-4">
                <table className={`w-100 rounded-3 position-relative mt-4 ${styles['table']} mx-auto`} id="table">

                    <thead className="py-2 px-2">
                        <tr>
                            <th className="px-5">{t('Name')}<Activateupdown /></th>
                            <th className="text-center">{t('Date')}<Activateupdown /></th>
                            <th className="text-center">{t('Settling Amount')}<Activateupdown /></th>
                            <th className="text-left"></th>
                        </tr>
                    </thead>

                    <tbody>
                        {(settledHistory.trips || []).map(({trip_id,driver_name,driver_settlement_date,driver_balance},i)=>{
                            return  <tr key={trip_id}>
                            <td>
                                <div className="d-flex justify-content-start align-items-center">
                                    <Avatar className={`${styles['avatar']}`} variant="circle">{driver_name.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                   {driver_name}
                                </div>

                            </td>
                            <td className="text-center">{getDate(driver_settlement_date)}</td>
                            <td className="text-center"><INRIcon /> {driver_balance}</td>
                            <td className='text-left'><ArrowRight /></td>
                        </tr>
                        })}
                       
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="7" className="text-center">
                                {/* {loading ? <PulseLoader size={15} margin={2} color="#36D7B7" /> : <Button >Load More</Button>} */}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </>
    )
}

export default History
