import { API, Storage } from "aws-amplify";
import moment from "moment";
import { ENVIRONMENT_MODE } from './GetEnv';

async function getTrips(token, status, from_date = null, to_date = null, customerId = null, init = false) {
    if (from_date === null || from_date === "" || to_date === null || to_date === "") {

        from_date = null;
        to_date = null;
    }
    else {
        from_date = ((new Date(moment(from_date).format("YYYY-MM-DD")).getTime()) / 1000);
        to_date = ((new Date(moment(to_date).add(1, 'days').format("YYYY-MM-DD")).getTime()) / 1000);
    }
    let params = {
        "token": token,
        "key": "PK",
        "status": status,
        "from_date": from_date,
        "to_date": to_date,
    }
    if (customerId !== null) {
        params['customer_uid'] = customerId;
    }
    try {
        let TripsResponse = await API.post(ENVIRONMENT_MODE, '/get_trips', {
            body: params
        });
        let trips = TripsResponse.trips;
        let responseToken = TripsResponse.token;
        if (init) {
            while (trips.length <= 10 && (responseToken !== "[]" && responseToken !== "")) {
                params["token"] = responseToken;
                try {
                    let TempTripResponse = await API.post(ENVIRONMENT_MODE, '/get_trips', {
                        body: params
                    });
                    trips = [...trips, ...TempTripResponse.trips];
                    responseToken = TempTripResponse.token;
                } catch (err) {
                    break;
                }
            }
        }

        return { token: responseToken, trips: trips }
    } catch (err) {
        return false;
    }
}

async function deleteTrip(id) {
    try {
        return await API.post(ENVIRONMENT_MODE, '/delete_trip', {
            body: {
                trip_id: id
            }
        });
    } catch (err) {
        return false;
    }
}

async function getTripDetails(id) {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_trip_details', {
            body: {
                trip_id: id
            }
        });
    } catch (err) {
        return false;
    }
}

async function getImage(s3_key){
    try {
        return await Storage.get(s3_key,{
            level: 'private',
                customPrefix: {
                  private: `prod/private/`
               },
        });
    } catch (err) {
        return false;
    }
}

async function CreateTransactionAdvance(tripDetails, role, paymentDetails) {
    try {
        let params = {
            role: role,
            customer_name: tripDetails.customer_name,
            customer_phone: tripDetails.customer_phone,
            trip_id: tripDetails.trip_id,
            mode: paymentDetails.paymentMode,
            advance_amount: parseInt(paymentDetails.paymentAmount),
            advance_date: (new Date(moment(paymentDetails.paymentDate).format("YYYY-MM-DD")).getTime()) / 1000,
            advance_note: paymentDetails.paymentNote
        }

        if (paymentDetails.blob) {
            const response = await Storage.put(`payment-doc/${paymentDetails.blob.name}`, paymentDetails.blob, {
                contentType: paymentDetails.blob.type,
                customPrefix: {
                    private: `prod/private/`
                },
                level: 'private'
            });
            params['s3_key'] = response.key
        }
        // console.log(params)
        return await API.post(ENVIRONMENT_MODE, '/create_transaction', {
            body: params
        });
    } catch (err) {
        return false;
    }
}

async function editTransactionAdvance(payload={}){
    try{
     let params = {
         trip_id : payload.trip_id ,
         transaction_id : payload.transaction_id,
         advance_amount : parseInt(payload.paymentAmount),
         mode : payload.paymentMode,
         advance_date: (new Date(moment(payload.paymentDate).format("YYYY-MM-DD")).getTime()) / 1000,
         advance_note: payload.paymentNote
     }
      
     if (payload.blob) {
         const response = await Storage.put(`payment-doc/${payload.blob.name}`, payload.blob, {
             contentType: payload.blob.type,
             customPrefix: {
                 private: `prod/private/`
             },
             level: 'private'
         });
         params['s3_key'] = response.key
     }
     
     return await API.post(ENVIRONMENT_MODE,'/edit_transaction',{body:params})

    }catch(e){
        return false
    }

 }

 async function editTransactionExpense(payload={}){
    try{
     let params = {
         trip_id : payload.trip_id ,
         transaction_id : payload.transaction_id,
         expense_amount : parseInt(payload.expenseAmount),
         mode : payload.expenseMode,
         expense_type : payload.expenseType,
         expense_date: (new Date(moment(payload.expenseDate).format("YYYY-MM-DD")).getTime()) / 1000,
         expense_note: payload.expenseNote
     }
      
     if (payload.blob) {
         const response = await Storage.put(`payment-doc/${payload.blob.name}`, payload.blob, {
             contentType: payload.blob.type,
             customPrefix: {
                 private: `prod/private/`
             },
             level: 'private'
         });
         params['s3_key'] = response.key
     }
     
      return await API.post(ENVIRONMENT_MODE,'/edit_transaction',{body:params})
    }catch(e){
        return false
    }

 }

 async function deleteTransactionAdvance(payload={}){
    try{
     let params = {
         trip_id : payload.trip_id ,
         transaction_id : payload.transaction_id ,
         advance_amount : 0,
      }
     return await API.post(ENVIRONMENT_MODE,'/edit_transaction',{body:params})
    }catch(e){
        return false
    }

 }

async function CreateTransactionExpense(tripDetails, role, expenseDetails) {
    try {
        let params = {
            role: role,
            customer_name: tripDetails.customer_name,
            customer_phone: tripDetails.customer_phone,
            trip_id: tripDetails.trip_id,
            mode: expenseDetails.expenseMode,
            expense_type: expenseDetails.expenseType,
            expense_amount: parseInt(expenseDetails.expenseAmount),
            expense_date: (new Date(moment(expenseDetails.expenseDate).format("YYYY-MM-DD")).getTime()) / 1000,
            expense_note: expenseDetails.expenseNote
        }
        if (expenseDetails.blob) {
            const response = await Storage.put(`payment-doc/${expenseDetails.blob.name}`, expenseDetails.blob, {
                contentType: expenseDetails.blob.type,
                customPrefix: {
                    private: `prod/private/`
                },
                level: 'private'
            });
            params['s3_key'] = response.key
        }
        // console.log(params)
        return await API.post(ENVIRONMENT_MODE, '/create_transaction', {
            body: params
        });
    } catch (err) {
        return false;
    }
}

async function getExpense() {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_expense_types', {});
    } catch (err) {
        return false;
    }
}

async function getBill(idx) {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_bill', {
            body: {
                trip_id: idx
            }
        })
    } catch (err) {
        return false;
    }
}

async function getShortLivedUrl(s3_key, type) {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_short_lived_url', {
            body: {
                customPrefix: {
                    private: `prod/private/`
                },
                s3_key: s3_key,
                type: type
            }
        })
    } catch (err) {
        return false;
    }
}

async function getCustomerTripPdf(from_date, to_date, customerUid, customerName) {
    try {
        const fromDateUnix = Math.ceil(fromDate.startOf('month').valueOf() / 1000);
        const toDateUnix = Math.ceil(toDate.endOf('month').valueOf() / 1000);
        return await API.post(ENVIRONMENT_MODE, '/get_customer_trips_pdf', {
            body: {
                "from_date": fromDateUnix,
                "to_date": toDateUnix,
                "customer_name": customerName,
                "customer_uid": customerUid
            }
        })
    } catch (error) {
        return false
    }
}



async function searchTrips(search_term, status, from_date = null, to_date = null) {
    if (from_date === null || from_date === "" || to_date === null || to_date === "") {

        from_date = null;
        to_date = null;
    }
    else {
        from_date = ((new Date(from_date).getTime()) / 1000);
        to_date = ((new Date(moment(new Date(to_date)).add(1, 'days').format("YYYY-MM-DD")).getTime()) / 1000);
    }


    try {
        let searchTripsResponse = await API.post(ENVIRONMENT_MODE, '/search_trips', {
            body: {
                "search_term": search_term,
                "status": status,
                "from_date": from_date,
                "to_date": to_date
            }
        });
        return searchTripsResponse.trips;
    } catch (err) {
        return false;
    }
}

export { getTrips, searchTrips, deleteTrip, getTripDetails, CreateTransactionAdvance, getExpense, CreateTransactionExpense, getBill, getCustomerTripPdf, getShortLivedUrl , editTransactionAdvance , getImage , deleteTransactionAdvance , editTransactionExpense}