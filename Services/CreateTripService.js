import API from "@aws-amplify/api";
import moment from "moment";
import { ENVIRONMENT_MODE } from './GetEnv';

async function getAllCities() {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_cities', {});
    } catch (err) {
        return false;
    }
}
 
async function createTrip(tripDetails, role) {
    let params = {
        customer_name: tripDetails.customerName,
        customer_phone: `+91${tripDetails.customerNumber}`,
        origin_city: tripDetails.origin,
        destination_city: tripDetails.destination,
        role: role,
        billing_type: tripDetails.billingType.replace('-', '_'),
        freight_amount: parseInt(tripDetails.freightAmount),
        trip_start_date: (new Date(moment(tripDetails.startDate).format("YYYY-MM-DD")).getTime()) / 1000
    }
    if (tripDetails.billingType !== "fixed") {
        params[`${tripDetails.billingType.split('-')[1]}`] = parseInt(tripDetails.total);
        params[`rate_${tripDetails.billingType.replace('-', '_')}`] = parseInt(tripDetails.rate);

    }
    if (tripDetails.startKmReading !== "") {
        params[`start_km_reading`] = parseInt(tripDetails.startKmReading)
    }
    if (tripDetails.truckNumber !== "") {
        params[`truck_id`] = tripDetails.truckNumber
    }
    if (tripDetails.driverName !== "") {
        params[`driver_name`] = tripDetails.driverName
    }
    if (tripDetails.driverPhone !== "") {
        params[`driver_phone`] = `+91${tripDetails.driverPhone}`
    }
    
    try{
        return await API.post(ENVIRONMENT_MODE,'/create_trip',{
            body: params
        })
    } catch (err) {
        return false;
    }
}

async function editTrip(tripDetails={}){
    let params = {
        customer_name: tripDetails.customerName,
        customer_phone: `+91${tripDetails.customerNumber}`,
        origin_city: tripDetails.origin,
        destination_city: tripDetails.destination,
        billing_type: tripDetails.billingType.replace('-', '_'),
        freight_amount: parseInt(tripDetails.freightAmount),
        trip_start_date: (new Date(moment(tripDetails.startDate).format("YYYY-MM-DD")).getTime()) / 1000,

        trip_id : tripDetails.trip_id

    }
    if (tripDetails.billingType !== "fixed") {
        params[`${tripDetails.billingType.split('-')[1]}`] = parseInt(tripDetails.total);
        params[`rate_${tripDetails.billingType.replace('-', '_')}`] = parseInt(tripDetails.rate);
    }
    if (tripDetails.startKmReading !== "") {
        params[`start_km_reading`] = parseInt(tripDetails.startKmReading)
    }
    if (tripDetails.truckNumber !== "") {
        params[`truck_id`] = tripDetails.truckNumber
    }
    if (tripDetails.driverName !== "") {
        params[`driver_name`] = tripDetails.driverName
    }
    if (tripDetails.driverPhone !== "") {
        params[`driver_phone`] = `+91${tripDetails.driverPhone}`
    }

    var payLoads = {} 
    for(let k in params){
        if(params[k]){
            payLoads[k] = params[k]
        }
    }
    try {
        return await API.post(ENVIRONMENT_MODE, '/edit_trip', {
            body: payLoads
        })
    } catch (err) {
        return false;
    }
}

export { getAllCities, createTrip , editTrip }