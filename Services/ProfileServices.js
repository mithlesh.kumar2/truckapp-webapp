import API from "@aws-amplify/api";
import { ENVIRONMENT_MODE } from './GetEnv';

async function updateUser(name,email,businessName){
    try{
        return await API.post(ENVIRONMENT_MODE,'/update_user',{
            body: {
                "name": name,
                "email": email,
                "business_name":businessName
            }
        })
    }catch(err){
        return false;
    }
}

export {updateUser}