import { Storage, API } from 'aws-amplify'
import moment from 'moment'
import { ENVIRONMENT_MODE } from './GetEnv';

async function getDriversList(payload={}){
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_summary', {
            body: {
                // "name": '',
                "type": 'driver',
                ...payload
                // "token  ": { // pagination ("[]" or "PK and SK")
                //     "PK  ": "to_uid:ap-south-1:e0aff076-8ead-4c3d-b993-1211fcff5adc  ",
                //     "SK  ": "from_uid:ap-south-1:270fb51c-e9f5-442c-b9e2-7b63626fcbf5  "
                // }
            }
        })
    } catch (error) {
        return false
    }
}




async function getAllTrips(payload={}){
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_trips', {
            body: {
                ...payload
            }
        })
    } catch (error) {
        return false
    }
}


async function get_transactions(payload={}){
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_transactions', {
            body: {
                ...payload
            }
        })
    } catch (error) {
        return false
    }
}


async function UpsertDriverAdvance(payload={}){
    try {
         const main = await API.post(ENVIRONMENT_MODE, '/upsert_driver_advance', {
            body: payload
        })
        return main
    } catch (error) {
        console.log('error = ',error)
        return false
    }
}





export { getDriversList , getAllTrips , UpsertDriverAdvance , get_transactions }