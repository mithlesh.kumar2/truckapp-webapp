import { API } from "aws-amplify";
import { ENVIRONMENT_MODE } from './GetEnv';

async function getAllTrucks() {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_trucks', {
            body: {
                "is_available": "all"
            }
        })
    } catch (err) {
        return false;
    }
}

async function getTruckDetails(id) {
    try {
        const truckDetails = await API.post(ENVIRONMENT_MODE, '/get_truck_details', {
            body: {
                "truck_id": id,
            }
        })
        return truckDetails
    } catch (err) {
        return false;
    }
}

async function getTruckTypes() {
    try {
        return await API.post(ENVIRONMENT_MODE, '/get_truck_types', {});
    } catch (err) {
        return false;
    }
}

async function createTruck(truckDetails) {
    let sNumber = truckDetails?.supplierNumber
    if(sNumber){
        sNumber = String(sNumber.indexOf('+91') == -1 ? '+91'+sNumber : sNumber)
    }

    let params = {
        container_type: truckDetails.containerType,
        truck_type: truckDetails.truckType,
        truck_number: truckDetails.truckNumber,
        ...(truckDetails.truckType==="Market Truck" ? {
            supplier_name : String(truckDetails.supplierName),
            supplier_phone : sNumber,
            ...(truckDetails?.supplierCity ? { supplier_city : String(truckDetails.supplierCity) } : {}),
            ...(truckDetails?.truckOwned ? { trucks_owned : Number(truckDetails.truckOwned) } : {}),
        } : {}),

        ...(truckDetails?.loadCapacity ? {capacity : String(truckDetails.loadCapacity)} : {}),
        ...(truckDetails?.bodyLength ? {body_length : String(truckDetails.bodyLength)} : {})
        
    }

    try {
        return await API.post(ENVIRONMENT_MODE, '/create_truck', {
            body: params
        })
    } catch (err) {
        return err;
    }
}


async function getStaticList(subtype) {
    let params = {
        "type": "body_length_and_capacity", // (REQUIRED)
        "subtype": [subtype] // (required) ['Mini truck/ LCV', 'Open Body Truck', 'Closed Container', 'Full Load Truck']
    }

    try {
        return await API.post(ENVIRONMENT_MODE, '/get_static_list', {
            body: params
        })
    } catch (err) {
        return err;
    }
}

async function deleteTruck(truckNumber) {
    try {
        return await API.post(ENVIRONMENT_MODE, '/delete_truck', {
            body: {
                "truck_number": truckNumber
            }
        })
    } catch (error) {
        return false
    }
}

export { getAllTrucks, getTruckTypes, createTruck, deleteTruck , getTruckDetails , getStaticList}