import { Storage, API } from 'aws-amplify'
import { currentUser } from './AuthServices';
import moment from 'moment'
import { ConsoleLogger } from '@aws-amplify/core';
import { ENVIRONMENT_MODE } from './GetEnv';

const S3Upload = async (blob, progressCallback) => {
    // const response = await Storage.put(fileName, blob, {
    //     contentType: contentType,
    //     customPrefix: {
    //         private: `prod/private/`
    //     },
    //     level: 'private',
    //     progressCallback: progress => {
    //         if (progressCallback) {
    //             progressCallback(progress.loaded, progress.total)
    //         }
    //         // console.log(`Uploaded: ${progress.loaded}/${progress.total}`);
    //     }
    // })

    const response = await Storage.put(`pod/${blob.name}`, blob, {
        contentType: blob.type,
        customPrefix: {
            private: `prod/private/`
        },
        level: 'private'
    });

    return response.key
}

const SubmitPod = (tripId, date, key) => {
    try {
        return API.post(ENVIRONMENT_MODE, '/submit_pod', {
            body: {
                "trip_id": tripId,
                "date": Math.floor(date.valueOf() / 1000),
                "s3_key": key
            }
        })
    } catch (error) {
        return false
    }
}

// const ViewPod = async (key) => {
//     try {
//         const signedURL = await Storage.get(key, {
//             level: 'private',
//             customPrefix: {
//                 private: `prod/private/`
//             },
//         });
//         return signedURL
//     } catch (error) {
//         return false
//     }
// }

const getPodUrl = async (key, type) => {
    try {
        const signedURL = await Storage.get(key, {
            level: 'private',
            customPrefix: {
                private: `prod/private/`
            },
        });

        return signedURL
    } catch (error) {
        return false
    }
}


const getTripPOD= async (pod={})=>{
   function isValid(dist={}){
    const res = {},
    schema = {
        ascending : 'boolean',
        from : 'number',
        to : 'number',
    },
    inSchema = (key,val)=>{
        var res = false
        for(let k in schema){
            if(key==k){
                if(schema[k]==typeof val){
                    res = true
                }
                break;
            }
        }
        return res
    },
    isExistsAnyKeyValue = (dist={})=>{
        var res = false
        for(let d in dist){
            let sc = inSchema(d,dist[d])
            if(dist[d] || sc){
                res = true
                break;
            }
        }
        return res
    },
    main = (dist,key)=>{
       for(let k in dist){
            if('object'===typeof dist[k]){
              if(isExistsAnyKeyValue(dist[k])){
                res[k] = {}
                main(dist[k],k)
              }
            }else{
                let v = dist[k] , sc = inSchema(k,v)
                if(v || sc){
                    if(key)res[key][k] = v;
                    else res[k] = v
                }
            }
       }
    }

    main(dist)
     return res
    }

        const payload =  isValid({
            filters: {
                status: pod?.status || "all",
                // balance_range: {
                //     from: 0,  
                //     to: 0
                //   }
            },
            sort: {
                on: pod?.sort?.on || "pod_date",
                ascending: Boolean(pod?.sort?.ascending)
              },
            search: {
                term: pod?.search
            },
            from: pod?.from 
        })

            
        if(pod.fromDate && pod.toDate){
            payload['filters']['date_range'] = {
                    from:  ((new Date(moment(new Date(pod.fromDate)).format("YYYY-MM-DD")).getTime()) / 1000),
                    to:  ((new Date(moment(new Date(pod.toDate)).format("YYYY-MM-DD")).getTime()) / 1000)
               }
         }else if(pod.buttonDate){
            payload['filters']['date_range'] = {
                from:  (new Date(moment().subtract(pod.buttonDate, 'days').format('YYYY-MM-DD')).getTime() / 1000),
                to:  (new Date(moment().format('YYYY-MM-DD'))).getTime() / 1000
            } 
         }
       
        try {
            return await API.post(ENVIRONMENT_MODE, '/get_trips_pod', {
                body: payload
            })
        } catch (error) {
            console.log('error = ',error)
            return false
        }

}

export { S3Upload, SubmitPod, getPodUrl , getTripPOD }