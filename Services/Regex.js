
export const VehicalRegistration_Number_Regex = new RegExp('^[A-Z]{2}[A-Z0-9]{3,10}$')
export const GST_Number_Regex = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}[A-Z]{1}[0-9A-Z]{1}$')
export const Pan_Number_Regex = new RegExp('^[A-Z]{5}[0-9]{4}[A-Z]{1}$')
export const Eway_Number_Regex = new RegExp('^[0-9]{12}$')


