import awsmobile from "./../Components/aws-exports"

const ENVIRONMENT_MODE = process.env.NEXT_PUBLIC_ENVIRONMENT_MODE


var API_ENDPOINT = ''
for(let k of (awsmobile.API.endpoints || [])){
    if('object'==typeof k){
        const { name , endpoint } = k
        if(name==ENVIRONMENT_MODE){
            API_ENDPOINT = endpoint
            break;
        }
    }
}

export { API_ENDPOINT , ENVIRONMENT_MODE }
