import { useState } from 'react'
import { Button, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import AddTruckModal from '../AddTruckModal';
import Link from 'next/link'

import styles from '../../../styles/Truck.module.scss'
import { useTranslation } from 'react-i18next';


const Operations = ({ trucks, getTrucks , filterHandler}) => {
    const { t } = useTranslation()
    const [addTruckModalOpen, setAddTruckModalOpen] = useState(false)
    
    let AddTruckModalClose = () => {
        setAddTruckModalOpen(false);
    }

    const handleFilterTrucks = (type='all')=>{
        let tempTrucks = []
        if(type==='all' || !type){
            tempTrucks = trucks
        }else{
            for(let truck of trucks){
                let t = String(truck.truck_type).toLowerCase().replace(' ','').trim()
                if(type===t){
                    tempTrucks.push(truck)
                }
            }
        }
        return tempTrucks
    }

    const handlerTrucksClick = (type)=>{
        filterHandler(type)
    }

    console.log("getTrucks = ",trucks) 

    return (
        <>
            <div className={`w-100 d-flex justify-content-between flex-wrap ${styles.operations}`}>

                <div className={`${styles.no_of_trucks}`} style={{cursor:'pointer'}} onClick={_=>handlerTrucksClick('all')}>
                    {t('All Trucks')} : {Array.isArray(trucks) ? trucks.length : '0'}
                    <div className={`d-flex justify-content-center align-items-center ${styles.truck_image}`}>
                        <img src="/truck.png" alt="" />
                    </div>
                </div>

                <div className={`${styles.no_of_trucks}`} style={{cursor:'pointer'}} onClick={_=>handlerTrucksClick('mytruck')}>
                    {t('My Trucks')} : {Array.isArray(trucks) ? handleFilterTrucks('mytruck').length : '0'}
                    <div className={`d-flex justify-content-center align-items-center ${styles.truck_image}`}>
                        <img src="/truck-green.svg" alt="" />
                    </div>
                </div>

                <div className={`${styles.no_of_trucks}`} style={{cursor:'pointer'}} onClick={_=>handlerTrucksClick('markettruck')}>
                    {t('Market Trucks')} : {Array.isArray(trucks) ? handleFilterTrucks('markettruck').length : '0'}
                    <div className={`d-flex justify-content-center align-items-center ${styles.truck_image}`}>
                        <img src="/truck-orange.svg" alt="" />
                    </div>
                </div>

                <Button onClick={() => setAddTruckModalOpen(true)} startIcon={<AddIcon />} variant='contained' color="primary">{t('Add Truck')}</Button>
            </div>
            <AddTruckModal open={addTruckModalOpen} truckListUpdate={getTrucks} close={AddTruckModalClose} />
        </>

    );
}

export default Operations;