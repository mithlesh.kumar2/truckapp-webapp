import { Fab } from '@material-ui/core'
import { LocalShippingOutlined, SentimentDissatisfiedOutlined } from '@material-ui/icons'
import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { PulseLoader } from 'react-spinners'
import ConfirmDelete from '../../ConfirmDelete';
import { MdDelete } from "react-icons/md";

import styles from '../../../styles/Truck.module.scss'
import { deleteTruck } from '../../../Services/TruckServices'
import { toast } from 'react-toastify'

const TruckTable = ({ trucks, getTrucks , truckType , onView }) => {

    const { t } = useTranslation()
    const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
    const [deleteTruckNo, setDeleteTruckNo] = useState()

    const cssClass = {
        'Mini truck/ LCV': 'mini_truck',
        'Open Body Truck': 'open_body',
        'Closed Container': 'closed_container',
        'Full Load Truck': 'full_load'
    }

    const handleDelete = async () => {

        try {
            const response = await deleteTruck(deleteTruckNo);
            if (response && response.success) {
                getTrucks();
                toast.success('truck deleted successfully')
            }
            else {
                toast.error(response.message)
            }

        } catch (error) {
            toast.error('Somethingn went wrong')
        }

        setOpenConfirmDialog(false);
    }

    return (
        <>
            {
                trucks === 'loading' ?
                    <div className="w-100 text-center"><PulseLoader size={15} margin={2} color="#36D7B7" /></div> :
                    <>
                        {
                            trucks.length === 0 ?
                                <h4 className={`text-center mt-5 no-data-found`}>No Data Found <SentimentDissatisfiedOutlined /></h4> :
                                <table className={`w-100 px-2 my-3 ${styles.table}`}>
                                    <thead>
                                        <tr>
                                            <th ></th>
                                            <th>Truck No</th>
                                            <th>{t('Truck Type')}</th>
                                            <th>{t('Truck Ownership')}</th>
                                            <th>{t('View Details')}</th>
                                            <th>{t('Delete Truck')}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            trucks.map(
                                                truck =>
                                                (truckType===String(truck.truck_type).toLowerCase().replace(' ','').trim() || !truckType || truckType==='all') ?
                                                    <tr className={`w-100 `} key={truck.truck_number}>
                                                        <td className={`mx-1`}>
                                                            <div className={`${styles.icon_bg} ${styles[cssClass[truck.container_type]]}`} ><LocalShippingOutlined className={``} /></div>
                                                        </td>

                                                        <td className={`mx-1`}>
                                                            {truck.truck_number}
                                                        </td>

                                                        <td className={`mx-1 ${styles[cssClass[truck.container_type] + '_text']}`}>
                                                            {t(truck.container_type)}
                                                        </td>

                                                        <td>
                                                            {t(truck.truck_type)}
                                                        </td>

                                                        <td onClick={_=>onView(truck.truck_number)}>
                                                            <img src="/eye.svg" />    
                                                        </td>

                                                        <td>
                                                            <div onClick={async () => { setDeleteTruckNo(truck.truck_number); setOpenConfirmDialog(true) }} style={{ height: '100%', cursor: 'pointer' }}>
                                                                <MdDelete />
                                                            </div>

                                                        </td>

                                                    </tr>
                                                : null
                                            )
                                        }

                                    </tbody>

                                </table>
                        }
                    </>
            }
            <ConfirmDelete open={openConfirmDialog} close={() => setOpenConfirmDialog(false)} action={handleDelete}
                message1='Delete the Truck ?'
                message2='If you delete the Truck will be gone forever . Are you sure want to proceed? '
            />
        </>
    );
}

export default TruckTable;