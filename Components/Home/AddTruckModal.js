import { Button, Checkbox, Dialog, DialogActions, FormControlLabel, FormGroup, InputLabel, FormControl, Icon, Radio, RadioGroup, TextField, Select, MenuItem } from '@material-ui/core'
import React, { useContext, useEffect, useState } from 'react'
import { PulseLoader } from 'react-spinners'
import { toast } from 'react-toastify'
import { GlobalLoadingContext } from '../../Context/GlobalLoadingContext'
import { createTruck, getTruckTypes, getStaticList } from '../../Services/TruckServices'
import { VehicalRegistration_Number_Regex } from '../../Services/Regex'
import styles from '../../styles/CreateTrip.module.scss'
import ClosedContainerTruckIcon from './svg/ClosedContainerTruck.svg'
import MiniTruckIcon from './svg/MiniTruck.svg'
import OpenBodyTruckIcon from './svg/OpenBodyTruck.svg'
import TrailerTruckIcon from './svg/TrailerTruck.svg'
import { ArrowDropDown } from '@material-ui/icons'

export default function AddTruckModal(props) {
    const [truckTypes, setTruckTypes] = useState([]);
    const [detailsIsValid, setDetailsIsValid] = useState(false);
    const { setGlobalLoading } = useContext(GlobalLoadingContext)
    const [truckDetails, setTruckDetails] = useState({
        truckType: "Market Truck",
        containerType: '',
        truckNumber: '',
        customerName: '',
        loadCapacity: '',
        bodyLength: '',

        supplierName: '',
        supplierNumber: '',
        supplierCity: '',
        truckOwned: ''
    })
    const [inputErrors, setInputErrors] = useState(false)
    const [staticList, setStaticList] = useState({
        body_length: [],
        capacity: []
    })
    let TruckIcons = [
        <MiniTruckIcon />,
        <OpenBodyTruckIcon />,
        <ClosedContainerTruckIcon />,
        <TrailerTruckIcon />
    ]

    const [bodyLengthOther, setBodyLengthOther] = useState(false)
    const [loadCapacityOther, setLoadCapacityOther] = useState(false)

    let Init = () => {
        setTruckDetails({
            truckType: "Market Truck",
            containerType: "",
            truckNumber: "",
            customerName: "",
            loadCapacity: '',
            bodyLength: '',
            supplierName: '',
            supplierNumber: '',
            supplierCity: '',
            truckOwned: ''
        })

    }

    let checkVehicalRegistrationNumber = (idx) => {
        return VehicalRegistration_Number_Regex.test(idx);
    }

    let HandleCreatetruck = async () => {
        setInputErrors(true)
        if (!detailsIsValid) {
            toast.error("invalid data");
            return;
        }

        setGlobalLoading(true);
        try {
            let createTruckResponse = await createTruck(truckDetails);
            if (createTruckResponse.success) {
                setGlobalLoading(false);
                if (props.truckListUpdate) {
                    props.truckListUpdate()
                }
                toast.success(createTruckResponse.message);
                Init();
                setInputErrors(false)
                props.close();
            }
            else {
                setGlobalLoading(false);
                toast.error("It seems like Truck Already Exists")
            }

        } catch (err) {
            setGlobalLoading(false)
        }
    }

    const getStaticListdata = async () => {
        if (truckDetails.containerType) {

            try {
                let list = await getStaticList(truckDetails.containerType)
                if ('object' === typeof list) {
                    setStaticList(list)
                }
            } catch (err) {

            }
        }
    }

    useEffect(() => {
        getStaticListdata()
    }, [truckDetails.containerType])

    useEffect(async () => {

        try {
            let getAllTruckTypes = await getTruckTypes();
            if (getAllTruckTypes) {
                setTruckTypes(getAllTruckTypes);
            }
        } catch (err) {

        }
    }, [])

    useEffect(() => {
        let isValid = true;
        let isMktT = truckDetails.truckType !== "Market Truck"

        Object.keys(truckDetails).map(key => {
            const ck = _ => {
                if (key !== 'customerName' && key !== "supplierCity" && key !== "truckOwned" && key !== "loadCapacity" && key !== "bodyLength" && truckDetails[key] === "") {
                    isValid = false;
                }
            }
            if (isMktT) {
                if (key === "supplierName" || key === "supplierNumber" || key === "supplierCity" || key === "truckOwned") {
                } else {
                    ck()
                }
            } else {
                ck()
            }

        })

        if (isValid) {
            isValid = checkVehicalRegistrationNumber(truckDetails.truckNumber)
        }

        if (isMktT) {
            if (truckDetails.supplierNumber && isValid) {
                isValid = String(truckDetails.supplierNumber).length === 10
            }
            if (isValid && truckDetails.truckOwned) {
                isValid = Number(truckDetails.truckOwned) >= -1
            }

        }

        setDetailsIsValid(isValid)
    }, [truckDetails])

    return (
        <Dialog fullWidth open={props.open} hideBackdrop disableBackdropClick onClose={props.close} maxWidth="lg">
            <form>
                <div className="row">
                    <div className="col-lg-5 col-md-5 col-11" style={{ marginLeft: 10 }}>
                        <h3 className="mt-3">Add Truck</h3>
                        <h6 className="mt-3">Select a truck Type</h6>

                        <RadioGroup className={`d-flex flex-column my-2 ${styles['truck-types']}`} value={truckDetails.containerType} onChange={(e) => setTruckDetails({ ...truckDetails, containerType: e.target.value })}>
                            {truckTypes.length <= 0 ? <PulseLoader size={15} margin={2} color="#36D7B7" /> : truckTypes.map((truck, i) =>
                                <div key={truck} className={`d-flex px-2 justify-content-between align-items-center py-1 ${styles[truck.toString().split(' ')[0].toLowerCase()]}`}>
                                    <Icon>{TruckIcons[i]}</Icon>
                                    {truck}
                                    <Radio value={truck} name="containerType" />
                                </div>
                            )}
                        </RadioGroup>


                        {
                            staticList?.capacity?.length ?
                            <>
                                {
                                    (loadCapacityOther) ?

                                        <div className="row pt-3">
                                            <div className="col-8">
                                                <TextField
                                                    variant="outlined"
                                                    label="Enter Load Capacity"
                                                    className="w-100"
                                                    value={truckDetails.loadCapacity}
                                                    onChange={(e) => setTruckDetails({ ...truckDetails, loadCapacity: e.target.value })}
                                                />
                                            </div>
                                            <div className="col-1 align-self-center" style={{ marginLeft: -25 }} onClick={_ => {
                                                setLoadCapacityOther(false)
                                                setTruckDetails({ ...truckDetails, loadCapacity: '' })
                                            }}>
                                                <ArrowDropDown />
                                            </div>
                                        </div>
                                        :
                                        <>
                                            <FormControl fullWidth style={{ paddingRight: 100, marginTop: 10 }}>
                                                <InputLabel id="select_Load_Capacity">Load Capacity</InputLabel>
                                                <Select
                                                    labelId="select_Load_Capacity"
                                                    label="Load Capacity"
                                                    className="w-100"
                                                    onChange={(e) => setTruckDetails({ ...truckDetails, loadCapacity: e.target.value })}
                                                >
                                                    {
                                                        (staticList?.capacity || []).map((v, i) => <MenuItem value={v} {...(String(v).toLowerCase() === 'other' ? { value: '', onClick: _ => setLoadCapacityOther(true) } : {})} key={v + i}>{v}</MenuItem>)
                                                    }
                                                </Select>
                                            </FormControl>
                                        </>
                                }
                            </>
                            : null

                        }




                        {
                            staticList?.body_length?.length ?
                            <>
                                {
                                    (bodyLengthOther) ?

                                        <div className="row pt-3">
                                            <div className="col-8">
                                                <TextField
                                                    variant="outlined"
                                                    label="Enter Body Length"
                                                    className="w-100"
                                                    value={truckDetails.bodyLength}
                                                    onChange={(e) => {
                                                        setTruckDetails({ ...truckDetails, bodyLength: e.target.value })
                                                    }
                                                    }
                                                />
                                            </div>
                                            <div className="col-1 align-self-center" style={{ marginLeft: -25 }} onClick={_ => {
                                                setBodyLengthOther(false)
                                                setTruckDetails({ ...truckDetails, bodyLength: '' })
                                            }}>
                                                <ArrowDropDown />
                                            </div>
                                        </div>
                                        :
                                        <>
                                            <FormControl fullWidth style={{ paddingRight: 100, marginTop: 10 }}>
                                                <InputLabel id="select_Load_Capacity">Body Length</InputLabel>
                                                <Select
                                                    llabelId="select_Body_Length"
                                                    label="Body Length"
                                                    onChange={(e) => setTruckDetails({ ...truckDetails, bodyLength: e.target.value })}
                                                >
                                                    {
                                                        (staticList?.body_length || []).map((v, i) => <MenuItem value={v} {...(String(v).toLowerCase() === 'other' ? { value: '', onClick: _ => setBodyLengthOther(true) } : {})} key={v + i}>{v}</MenuItem>)
                                                    }

                                                </Select>
                                            </FormControl>
                                        </>
                                }
                            </>
                            : null
                        }
                    </div>

                    <div className="col-lg-5 col-md-5">
                        <h3>Registration Number</h3>
                        <TextField
                            variant="outlined"
                            className="col-lg-8 col-md-8 col-11 mt-4 mx-auto"
                            label="Vehicle Registration Number"
                            required
                            error={inputErrors && (!(checkVehicalRegistrationNumber(truckDetails.truckNumber)) || truckDetails.truckNumber === "")}
                            value={truckDetails.truckNumber}
                            onChange={(e) => setTruckDetails({ ...truckDetails, truckNumber: e.target.value.toUpperCase() })}
                        />

                        <h5 className="mt-4">Ownership</h5>
                        <RadioGroup row value={truckDetails.truckType} onChange={(e) => setTruckDetails({ ...truckDetails, truckType: e.target.value })}>
                            <FormControlLabel label="Market Truck" control={<Radio name="truckType" style={{ color: "rgba(231, 104, 50, 1)" }} value="Market Truck" />}></FormControlLabel>
                            <FormControlLabel label="My Truck" control={<Radio name="truckType" style={{ color: "rgba(231, 104, 50, 1)" }} value="My Truck" />}></FormControlLabel>
                        </RadioGroup>


                        <h4 className="mt-3">Supplier Details</h4>
                        {
                            truckDetails.truckType === "Market Truck" && <>
                                <TextField
                                    variant="outlined"
                                    className="col-lg-8 col-md-8 col-11 mt-4 mx-auto"
                                    label="Supplier Name"
                                    required
                                    value={truckDetails.supplierName}
                                    onChange={(e) => setTruckDetails({ ...truckDetails, supplierName: e.target.value.toUpperCase() })}
                                />

                                <TextField
                                    variant="outlined"
                                    className="col-lg-8 col-md-8 col-11 mt-4 mx-auto"
                                    label="Supplier Number"
                                    required
                                    type="number"
                                    value={truckDetails.supplierNumber}
                                    onChange={(e) => setTruckDetails({ ...truckDetails, supplierNumber: e.target.value })}
                                />
                                <TextField
                                    variant="outlined"
                                    className="col-lg-8 col-md-8 col-11 mt-4 mx-auto"
                                    label="Supplier City"
                                    value={truckDetails.supplierCity}
                                    onChange={(e) => setTruckDetails({ ...truckDetails, supplierCity: e.target.value })}
                                />

                                <TextField
                                    variant="outlined"
                                    className="col-lg-8 col-md-8 col-11 mt-4 mx-auto"
                                    label="Truck Owned"
                                    min={1}
                                    type="number"
                                    value={truckDetails.truckOwned}
                                    onChange={(e) => setTruckDetails({ ...truckDetails, truckOwned: e.target.value })}
                                />

                            </>
                        }



                    </div>

                </div>



                <DialogActions className="mt-4 d-flex align-items-center">
                    <Button variant="outlined" onClick={props.close}>Close</Button>
                    <Button variant="contained" disabled={!detailsIsValid} onClick={HandleCreatetruck} color="primary">Confirm</Button>
                </DialogActions>

            </form>
        </Dialog>
    )
}
