import { Dialog, DialogTitle, Fab, DialogContent, DialogActions, Button } from "@material-ui/core";
import { MdDelete } from "react-icons/md";

export default function ConfirmDelete(props) {
    const { message1, message2 } = props
    return (
        <Dialog open={props.open || false} onClose={props.close} fullWidth className="confirm-dialog">
            <DialogTitle>
                <div className="d-flex align-items-center confirm-dialog-title">
                    <Fab className="confirm-dialog-title-icon">
                        <MdDelete style={{ fontSize: '30px' }} />
                    </Fab>
                    {/* Delete the Trip ? */}
                    {message1}
                </div>
            </DialogTitle>

            <DialogContent className="confirm-content">
                {/* If you delete the Trip will be gone forever . Are you sure want to proceed? */}
                {message2}
            </DialogContent>

            <DialogActions>
                <Button variant="contained" onClick={() => props.close()}>Cancel</Button>
                <Button variant="contained" color="secondary" onClick={props.action}>Yes</Button>
            </DialogActions>
        </Dialog>
    )
}