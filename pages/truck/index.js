import { useState, useEffect } from 'react'


import Operations from '../../Components/Home/Truck/Operations'
import TruckTable from '../../Components/Home/Truck/TruckTable'

import { currentUser } from '../../Services/AuthServices'
import { getAllTrucks , getTruckDetails } from '../../Services/TruckServices'
import styles from '../../styles/Truck.module.scss'


const TruckDetailsView = ({truckNo,isClose})=>{
    const [data,setData] = useState({})
    let handlerGetTruckDetails = async (id) => {
        try {
            let truckDetails = await getTruckDetails(id);
            setData(truckDetails)
        }
        catch (err) {

        }
    }

    useEffect(()=>{
        handlerGetTruckDetails(truckNo)
    },[truckNo])


    return <>
        <div className="container border p-3 m-4">
            <div className="row pt-3">  
                <div className="col-4 m-2 h4">Ownership Type</div>
                <div className="col-4 m-2 text-muted text-capitalize"><small>{data.truck_type}</small></div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Truck No</div>
                <div className="col-4 m-2 text-left">{data.truck_id}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Supplier Name</div>
                <div className="col-4 m-2 text-left">{data?.supplier_name || '-'}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Truck Type</div>
                <div className="col-4 m-2 text-left">{data?.container_type || '-'}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Supplier City</div>
                <div className="col-4 m-2 text-left text-capitalize">{data?.supplier_city||'-'}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Supplier Number</div>
                <div className="col-4 m-2 text-left">{data?.supplier_phone || '-'}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Truck Owned</div>
                <div className="col-4 m-2 text-left">{data?.trucks_owned || '-'}</div>
                <div className="w-100"></div>
            
            
            </div>

        </div>

        <div className="container border p-3 m-4">
            <h4>Other Details</h4>
            <div className="row ">  
                <div className="col-4 m-2">Load Capacity</div>
                <div className="col-4 m-2 text-left">{data?.capacity || '-'}</div>
                <div className="w-100"></div>

                <div className="col-4 m-2">Truck Length</div>
                <div className="col-4 m-2 text-left">{data?.body_length || '-'}</div>
                <div className="w-100"></div>
            </div>

            <div className="container text-center mt-4">
                <button className="btn btn-primary px-4" onClick={isClose}>Close</button>
            </div>

        </div>
    </>

}

export default function Truck() {

    const [user, setUser] = useState(currentUser.value);
    const [trucks, setTrucks] = useState('loading');
    const [truckDetailsView,isTruckDetailsView] = useState(false)

    let getTrucks = async () => {
        try {
            let allTrucks = await getAllTrucks();
            if (allTrucks) {
                setTrucks(allTrucks);
            }
        }
        catch (err) {
        }
    }
    

    useEffect(async () => {
        getTrucks()
    }, [])


    const props = { trucks, getTrucks , getTruckDetails }
    const [truckType,setTruckType] = useState('')

    return (
        <>
            {(user !== null && user !== "loading") &&
                truckDetailsView ?
                <TruckDetailsView truckNo={truckDetailsView} isClose={_=>isTruckDetailsView(false)} />
                :
                <div className={`w-100 h-100 px-lg-3 px-md-3 px-1 py-lg-3 py-5`}>
                    <Operations {...props} filterHandler={e=>setTruckType(e)} />
                    <TruckTable {...props} truckType={truckType} onView={isTruckDetailsView}/>
                </div>
            }
        </>
    )
}
