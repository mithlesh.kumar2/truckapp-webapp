import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useRouter } from 'next/router'
import { Button, TextField, Avatar } from '@material-ui/core';
import styles from './../../../styles/CustomerData.module.scss'
import Link from 'next/link'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { PulseLoader } from 'react-spinners'
import { getHistory } from '../../../Services/driverKhata'
import Activateupdown from '../../../Components/Home/svg/Activateupdown.svg'
import ArrowRight from './svg/arrowRight.svg'
import { Delete } from '@material-ui/icons'
import Eye from '../../../Components/Home/svg/Eye.svg'
import ConfirmDelete from '../../../Components/ConfirmDelete';
import { getAllTrips, UpsertDriverAdvance, get_transactions } from '../../../Services/driverKhata'
import moment from "moment";

const settleDriver = () => {
    const { t } = useTranslation()
    const [openConfirmDialog, setOpenConfirmDialog] = useState(false)
    const router = useRouter();
    const driver_uid = router.query.driver_uid
    const handleDelete = async () => {
        setOpenConfirmDialog(false)
    }

    const [tripData = {}, setTripDATA] = useState()
    const [allAprovedExp, setAllAprovedExp] = useState([])
    const [customSettledValue,setCustomSettledValue] = useState()

    var totalAdvance = 0, approvedExpenses = 0


    const AllTrips = async () => {
        console.log('trip_id = ', driver_uid)
        if (driver_uid) {
            try {
                const list = await getAllTrips({ driver_uid: driver_uid })
                if (list) {
                    console.log('list = ', list.trips[0])
                    setTripDATA(list.trips[0])

                }

            } catch (e) {
                console.log('e = ', e)
            }
        }

    }


    let getAllTransactions = async () => {
        if (tripData && tripData.trip_id) {
            try {
                const reulst = await get_transactions({
                    "trip_id": tripData.trip_id, // (required)
                    "type": "expense"
                })
                setAllAprovedExp(reulst)
                console.log("getAllTransactions = ", reulst)


            } catch (e) {
                console.log('reulst = ', e)
            }
        }
    }

    let handleSettement = async () => {
        let amount = 0
        let tcv = Number(customSettledValue)
        if(tcv || tcv===0){
            amount = tcv
        }else{
            amount = Math.abs( totalAdvance - approvedExpenses)
        }

        let payloads = {
            trip_id: tripData.trip_id,
            amount: Number(amount),
            reason: 'settlement reason',
            is_settled: true,
            date: (new Date(moment(Date.now()).format("YYYY-MM-DD")).getTime()) / 1000
        }

        try {
            const result = await UpsertDriverAdvance(payloads)
            // AllTrips()
            window.history.back()

            console.log('response_handleSettement = ', result)
        } catch (e) {
            console.log('error = ', e)
        }
    }

    useEffect(() => {
        getAllTransactions()
    }, [tripData])
    useEffect(() => {
        (async () => {
            AllTrips()
        })()
    }, [driver_uid])


    const handleCustomSettlement = (e)=>{
        setCustomSettledValue(e.target.value)
        console.log('value = ',)
    }

    const numType = n=>-1 >= n ? 'neg' : 'pos'
        
    return (
        !tripData ? null :
            <>
                <div className="mx-4 px-4 py-3" style={{ background: 'white' }}>

                    <div>
                        <div className="font-weight-bold h5">Settle Driver</div>
                        <div className='row text-secondary'>
                            {/* {
                                (tripData.driver_advance?.advances || []).map((v, i) => {

                                    return <React.Fragment key={v + i}>
                                        <div className='col'>Adavnce Paymennt</div>
                                        <div className='col-4' style={{ textAlign: 'right' }}>₹ 9,000/-</div>
                                        <div className='w-100'></div>
                                        <div className='col'>Extra Advance </div>
                                        <div className='col-4' style={{ textAlign: 'right' }}>₹ 9,000/-</div>
                                    </React.Fragment>
                                })
                            } */}

                        </div>
                        <hr style={{ borderTop: '1px dashed white' }} />
                        <div className='row'>
                            <div className='col'>Total  Advance </div>
                            <div className='col-4 ' style={{ textAlign: 'right' }}>₹ {tripData?.driver_advance?.total}</div>
                        </div>
                    </div>

                    <div className="pt-5">
                        <div className="font-weight-bold h5">ALL Approved Expenses</div>
                        <div className='row text-secondary'>
                            {
                                allAprovedExp.map((v, i) => {
                                    let amount = Math.abs(v.amount)
                                    if (v.status === "approved") {
                                        if(String(v.reason).toLocaleLowerCase().trim()==="advance"){
                                            totalAdvance += amount
                                        }else{
                                            approvedExpenses += amount
                                        }
                                       
                                        return <React.Fragment key={v + i}>
                                            <div className='col'>{v.reason} </div>
                                            <div className='col-4 d-flex justify-content-end' style={{ textAlign: 'right' }}>
                                                ₹ {amount}
                                                {/* <Link href="#">
                                                <div className='px-3' style={{ cursor: 'pointer' }}>
                                                    <Eye className='mx-1' />
                                                    <span style={{ textDecoration: 'underline' }}>View Details</span>
                                                </div>
                                                </Link> */}
                                                {/* <lete style={{cursor:'pointer'}} className='px-1 mx-1' onClick={_=>setOpenConfirmDialog(true)} /> */}
                                            </div>
                                            <div className='w-100'></div>
                                        </React.Fragment>
                                    } else return null
                                })
                            }
                            {
                                !allAprovedExp.length && <small>No expense</small>
                            }


                        </div>
                        <div className='row border mt-4 py-3 alert-link' style={{ color: '#080B40' }}>
                            <div className='col'>
                                Balance Amount<br />
                                <small style={{fontSize : 10}}>( Total Advance - Approved Expenses )</small>
                            </div>
                            <div className='col-4 align-self-center' style={{ textAlign: 'right', color : numType(totalAdvance-approvedExpenses)==='neg' ?  '#F53636' : '#2dbc53' }}>
                                <div><small style={{fontSize : 10}}>{
                                    numType(totalAdvance-approvedExpenses)==='neg' ? 'To Pay' : 'To Receive'
                                    }</small></div>
                                ₹ {Math.abs(totalAdvance - approvedExpenses)}</div>
                        </div>
                    </div>

                    <div className='row mt-4 py-3 alert-link' style={{ color: '#312968' }}>
                        <div className='col'>Custom Settlement</div>
                        <div className='col-3 align-self-center' style={{ textAlign: 'right' }}>
                            <TextField type="number" onChange={handleCustomSettlement} label={t('Enter Amount')} style={{ minWidth: '90%' }} />
                        </div>
                    </div>

                    <div className="text-center mt-5">
                        <Button onClick={handleSettement} className="bg-primary text-light px-4" size="medium">Settle Balance</Button>
                    </div>

                    {
                        openConfirmDialog &&
                        <ConfirmDelete open={openConfirmDialog} close={() => setOpenConfirmDialog(false)} action={handleDelete}
                            message1='Delete the Entry ?'
                            message2='If you delete this Entry will be gone forever . Are you sure want to proceed? '
                        />
                    }

                </div>
            </>
    )
}

export default settleDriver
