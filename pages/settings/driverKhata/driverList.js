import { useState,useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, TextField , Avatar} from '@material-ui/core';
import styles from './../../../styles/CustomerData.module.scss'
import Link from 'next/link'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { PulseLoader } from 'react-spinners'
import { getDriversList } from '../../../Services/driverKhata'

const driverList = () => {
    const { t } = useTranslation()
    const [totalDrivers,setTotalDrivers] = useState({
        summary:[],
        token : [],
        type : ''//search
    })
    const [loading,setLoading] = useState(false);
    const [search,setSearch] = useState('')

    let dList = async ()=>{
        try{
            const temp = {}
            if(totalDrivers.type==='search'){
                temp.name = search
            }
            if(totalDrivers.token.length){
                temp.token = totalDrivers.token
            }
            const list = await getDriversList(temp)
            console.log('list = ',list)
            setTotalDrivers({
                summary : list.summary,
                token : list.token ? JSON.parse(list.token) : [],
                type : totalDrivers.type,
            })
        }catch(e){
            console.log('e = ',e)
        }
    }

    let handleSearch = (e)=>{
        setSearch(e.target.value)
        setTotalDrivers({...totalDrivers,type:'search'})
    }

    useEffect(()=>{
        dList()
        
    },[search])
    const numType = n=>-1 >= n ? 'neg' : 'pos'

    return (
        <>
            <div className="row justify-content-between align-item-end">
                <div className="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-5">
                    <TextField onChange={handleSearch} value={search} label={t('Search Driver')} style={{ minWidth: '90%' }} />
                </div>
                <div className="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                    <Link href={`/settings/driverKhata/settlementHistory`}>
                        <Button variant="contained" color="primary" className="px-4" size='medium'>History</Button>
                    </Link>
                </div>
            </div>

            <div className="px-4">
                <table className={`w-100 rounded-3 position-relative mt-4 ${styles['table']} mx-auto`} id="table">

                    <thead className="py-2 px-2">
                        <tr>
                            <th className="px-5">{t('Driver Name')}</th>
                            <th className="text-center">{t('Balance')}</th>
                        </tr>
                    </thead>

                    <tbody>
                        {(totalDrivers.summary || []).map(({balance,from_name,from_uid,phone}) =>
                            <Link href={`/settings/driverKhata/driverTripList?id=${from_uid}`} key={from_uid}>
                                <tr>
                                <td>
                                    <div className="d-flex justify-content-start align-items-center">
                                        <Avatar className={`${styles['avatar']}`} variant="circle">{from_name.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                        {from_name}
                                    </div>
                                </td>
                                <td className="text-center" style={{color: numType(balance)==='neg' ?  '#F53636' : '#2dbc53'}}>
                                    ₹ {Math.abs(balance)}
                                </td>
                            </tr></Link>
                        )}

                    </tbody>
                    <tfoot>
                        <tr>
                            <td colSpan="7" className="text-center">
                                {/* {loading ? <PulseLoader size={15} margin={2} color="#36D7B7" /> :  <Button >Load More</Button>} */}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </>
    )
}

export default driverList
