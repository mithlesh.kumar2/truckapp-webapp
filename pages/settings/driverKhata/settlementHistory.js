import React from "react";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button, TextField,ButtonGroup,  Fab, Avatar } from '@material-ui/core';
import styles from './../../../styles/CustomerData.module.scss'
import Link from 'next/link'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { PulseLoader } from 'react-spinners'
import { getAllTrips, get_transactions } from '../../../Services/driverKhata'
import Activateupdown from '../../../Components/Home/svg/Activateupdown.svg'
import ArrowRight from './svg/arrowRight.svg'
import moment from 'moment'
import { ChevronRight  ,LocalShippingOutlined} from '@material-ui/icons'
import { getTripDetails} from '../../../Services/TripDataServices'
import tripStyles from './../../../styles/TripsData.module.scss'


const getDate = (milliseconds) => {
    return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
}

const AllDetails = ({ data = {}, close }) => {
    const numType = n => -1 >= n ? 'neg' : 'pos'
    const [tripData, setTripData] = useState({})
    const [expense,setExpense] = useState([])
    const [ToggleExp,setToggleExp] = useState(false)
    let getTrip = async () => {
        try {
            const trip = await getTripDetails(data.trip_id)
            if(trip){
                setTripData(trip)
               
            }else{
            }
        } catch (e) {
        }
    }
    let GetAllTransactions = async () => {
        if(data.trip_id){
            try {
                const list = await get_transactions({
                    type: "expense",
                    trip_id:data.trip_id// "linkedin_trip_id = ap-south-1:6376c2e2-a84a-4cd2-b014-8ecf0ef46c2f" 
                })
                setExpense(list)
            } catch (e) {

            }
        }
    }
    useEffect(()=>{
        getTrip()
        GetAllTransactions()
    },[data])

    return (
        <React.Fragment>
            <div>
                <table className={`w-100 rounded-3 position-relative mt-4 ${tripStyles['table']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
                    <thead>
                        <tr>
                            <th style={{ width: "1%" }}></th>
                            <th>Customer Name</th>
                            <th>Date</th>
                            <th>Route</th>
                            <th>Driver Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tripData &&
                            <tr style={{ cursor: "pointer" }} >
                                <td style={{ width: "1%" }}><Fab className={tripStyles[tripData.status]} ><LocalShippingOutlined className={tripStyles[tripData.status]} /></Fab></td>
                                <td className="text-left">{tripData?.customer_name || ""}</td>
                                <td className="text-center">{getDate(tripData.trip_start_date)}</td>
                                <td className="d-flex text-center justify-content-center align-items-center">
                                    <div className="d-flex py-1 flex-column justify-content-between align-items-start m-auto">
                                        <div className="d-flex align-items-center justify-content-start">
                                            <span className={tripStyles['dot']} style={{ backgroundColor: "rgba(45, 188, 83, 1)" }}></span>
                                            <span className="mx-1">{tripData.origin_city}</span>
                                        </div>
                                        <span className={tripStyles["vertical-line"]}></span>
                                        <div className="d-flex align-items-center justify-content-start">
                                            <span className={tripStyles["dot"]} style={{ backgroundColor: "rgba(231, 104, 50, 1)" }}></span>
                                            <span className="mx-1">{tripData.destination_city}</span>
                                        </div>
                                    </div>
                                </td>
                                <td className="text-right">{tripData.driver_name}</td>

                            </tr>
                        }
                    </tbody>
                </table>
            </div>
            <div className="container px-4">
                <h3>Payments</h3>
                <div className="pl-10">
                    {(data?.driver_advance?.advances || []).map((v, i) => {
                        let t = numType(v.amount)
                        return (
                            <div className="row py-3" key={v + i}>
                                <div style={{paddingLeft:140}} className="col-3 text-capitalize">{v.reason}</div>
                                <div style={{paddingLeft:70}} className="col-4 text-center">{getDate(v.date)}</div>
                                <div style={{paddingLeft:40}} className="col-3 ">{t === 'neg' ? 'Paid' : 'Received'}</div>
                                <div className="col-2 " style={{ color: t === 'neg' ? '#F53636' : '#2dbc53' }}>₹ {Math.abs(v.amount)}</div>
                            </div>
                        )
                    })}

                    <div className="container pt-10 my-3 border pb-2">
                        <div className="row py-3 px-3" style={{fontWeight : 'bold'}}>
                            <div className="col">Total Paid</div>
                            <div className="col text-right" style={{textAlign:'right',marginRight : 80}}>₹ {data?.driver_advance?.total}</div>
                        </div>
                    </div>
                    <div className="container pt-10 my-3 border pb-2">
                            <div className="row font-weight-bold mx-3 mt-2" style={{cursor:'pointer',fontWeight : 'bold'}} onClick={_=>setToggleExp(!ToggleExp)}>All Approved Expenses</div>
                            
                            <div className="container py-3 px-3" style={{display : ToggleExp ? 'block' : 'none'}}>
                                {
                                    expense.map((v,i)=>{
                                        if(v.status==="approved"){

                                            return(
                                                <div className="row">
                                                    <div className='col text-left'>{v.reason}</div>
                                                    <div className='col text-center'>{getDate(v.date)}</div>
                                                    <div className='col text-right'>₹ {Math.abs(v.amount)}</div>
                                                </div>
                                            )

                                        }else return null
                                    })
                                }

                            </div>
                    </div>
                </div>

            </div>
            <div className="text-center py-5 pt-10">
                <Button variant="contained" onClick={_ => close()} className="px-4 bg-primary">Close</Button>
            </div>
        </React.Fragment>
    )
}


const History = () => {
    const { t } = useTranslation()
    const [settledHistory, setSettledHistory] = useState({
        from: null,
        trips: [],
        type: ''//search
    })
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState('')
    const [isFullDetails, setIsFullDetails] = useState()

    

    let driverList = async () => {
        try {
            const temp = {
                // "type": "driver_settled",
                driver_uid: 'ap-south-1:dce7d742-7668-492d-9f03-f50769c84428',
                "trip_id": "trip16310385182811556-632-ap-south-1:9d440824-f443-4091-8d01-fd85c912ab50",
                type: 'expense'
            }
            // if(totalDrivers.type==='search'){
            //     temp.name = search
            // }
            // if(totalDrivers.token.length){
            //     temp.token = totalDrivers.token
            // }
            const list = await getAllTrips({
                type: "driver_settled"
            })
            setSettledHistory({ ...settledHistory, ...list })
            // await 
            console.log('response    = ', list)

            // setTotalDrivers({
            //     summary : list.summary,
            //     token : list.token ? JSON.parse(list.token) : [],
            //     type : totalDrivers.type,
            // })
        } catch (e) {
            console.log('e = ', e)
        }
    }

    let handleSearch = (e) => {
        setSearch(e.target.value)
        setTotalDrivers({ ...totalDrivers, type: 'search' })
    }

    const handleFullDetails = (data) => {
        setIsFullDetails(data)
    }

    useEffect(() => {
        driverList()
    }, [search])

    return (
        isFullDetails ? <AllDetails data={isFullDetails} close={_ => handleFullDetails()} />
            :
            <>
                {/* <div className="row justify-content-between align-item-end">
                    <div className="col-lg-6 col-md-8 col-sm-12 col-xs-12 px-5">
                        <TextField onChange={handleSearch} value={search} label={t('Search Driver Name')} style={{ minWidth: '90%' }} />
                    </div>

                </div> */}

                <div className="px-4">
                    <table className={`w-100 rounded-3 position-relative mt-4 ${styles['table']} mx-auto`} id="table">
                        <thead className="py-2 px-2">
                            <tr>
                                <th className="px-5">{t('Name')}</th>
                                <th className="text-center">{t('Date')}</th>
                                <th className="text-center">{t('Settling Amount')}</th>
                                <th className="text-left"></th>
                            </tr>
                        </thead>

                        <tbody>
                            {(settledHistory.trips || []).map((V, i) => {
                                const { trip_id, driver_name, driver_settlement_date, driver_balance } = V
                                return <tr key={trip_id}>
                                    <td>
                                        <div className="d-flex justify-content-start align-items-center">
                                            <Avatar className={`${styles['avatar']}`} variant="circle">{driver_name.split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                            {driver_name}
                                        </div>

                                    </td>
                                    <td className="text-center">{getDate(driver_settlement_date)}</td>
                                    <td className="text-center"><INRIcon /> {driver_balance}</td>
                                    <td className='text-left' ><ChevronRight onClick={_ => handleFullDetails(V)} /></td>
                                </tr>
                            })}

                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan="7" className="text-center">
                                    {/* {loading ? <PulseLoader size={15} margin={2} color="#36D7B7" /> : <Button >Load More</Button>} */}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </>
    )
}

export default History
