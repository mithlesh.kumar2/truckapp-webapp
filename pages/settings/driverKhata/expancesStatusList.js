import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from "react-i18next";
import { Button, ButtonGroup, Fab, Avatar } from '@material-ui/core'
import { LocalShippingOutlined , ChevronRight } from '@material-ui/icons'
import tripStyles from './../../../styles/TripsData.module.scss'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { getAllTrips } from '../../../Services/driverKhata'
import moment from 'moment'
import { blue, green } from '@material-ui/core/colors'
import { DatePicker } from '@material-ui/pickers'
import styles from './../../../styles/CustomerData.module.scss'
import { get_transactions } from '../../../Services/driverKhata'
import { getTripDetails , getShortLivedUrl} from '../../../Services/TripDataServices'
import AddPaymentMadeModal from './../../../Components/Home/Trip/TripsData/TripSummary/AddPaymentMadeModal'

const FullViewExpStatus = ({data,close})=>{

    const [tripData, setTripData] = useState({})
    const [ image,setImage] = useState()
    let getTrip = async () => {
        try {
            const trip = await getTripDetails(data.trip_id)
            if(trip){
                setTripData(trip)
               
            }else{
            }
        } catch (e) {
        }
    }

    let imgUrl = async (pod_s3_key)=>{
        if(pod_s3_key){
            const image = await getShortLivedUrl(pod_s3_key,'pod')
            if(image && image.success){
                setImage({...trip,image:image.link})
            }
        }
    }

    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }

    const numType = n=>-1 >= n ? 'neg' : 'pos'
  
 useEffect(() => {
     console.log("data= ",data)
        getTrip()
        if(data.s3_key){
            imgUrl(data.s3_key)
        }
    }, [data])

    return <React.Fragment>
        <table className={`w-100 rounded-3 position-relative mt-4 ${tripStyles['table']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
            <thead>
                <tr>
                    <th style={{ width: "1%" }}></th>
                    <th>Customer Name</th>
                    <th>Cotact No.</th>
                    <th>Truck No.</th>
                    <th>Route</th>
                    <th>Balance Amount</th>
                </tr>
            </thead>
            <tbody>
                {tripData &&
                    <tr style={{ cursor: "pointer" }} >
                        <td style={{ width: "1%" }}><Fab className={tripStyles[tripData.status]} ><LocalShippingOutlined className={tripStyles[tripData.status]} /></Fab></td>
                        <td>{tripData?.customer_name || ""}</td>
                        <td>{tripData.customer_phone}</td>
                        <td>{tripData.truck_number}</td>
                        <td className="d-flex justify-content-center align-items-center">
                            <div className="d-flex py-1 flex-column justify-content-between align-items-start m-auto">
                                <div className="d-flex align-items-center justify-content-start">
                                    <span className={tripStyles['dot']} style={{ backgroundColor: "rgba(45, 188, 83, 1)" }}></span>
                                    <span className="mx-1">{tripData.origin_city}</span>
                                </div>
                                <span className={tripStyles["vertical-line"]}></span>
                                <div className="d-flex align-items-center justify-content-start">
                                    <span className={tripStyles["dot"]} style={{ backgroundColor: "rgba(231, 104, 50, 1)" }}></span>
                                    <span className="mx-1">{tripData.destination_city}</span>
                                </div>
                            </div>
                        </td>
                        <td><INRIcon className="inr-icon" /> {tripData.freight_amount}</td>
                    </tr>
                }
            </tbody>
        </table>
        <div>
            <div style={{ border:'1px solid',borderRadius : 5 }} className="d-flex align-items-center justify-content-evenly flex-wrap py-1 mt-3">
                     Status:<span className="text-capitalize" style={{color: 'green'}}>{String(data.status).trim().replace('_',' ')}</span>
            </div>

            <div className='container px-5 pt-5'>
            <div className="row">
                    <div className="col">
                     <div>Expense Date & Time</div>
                        <div style={{fontWeight : 'bold'}}>{data.reason}</div>
                    </div>
                    <div className="col">
                        <div>Expense Date & Time</div>
                        <div style={{fontWeight : 'bold'}}>{getDate(data.date)}</div>
                    </div>
                        {
                            image && <div className="col"><img src={image} width={50} /></div>
                        }
                    
            </div>
                <div className="row font-weight-bold pt-5">
                    <div className="col">Amount : </div>
                    <div className="col text-right" style={{ color : numType(data.amount)==='neg' ? '#2dbc53' : '#F53636' }}>₹ {Math.abs(data.amount)}</div>
                </div>
            </div>
        </div>
        <div className="text-center py-5 pt-10">
            <Button variant="contained" onClick={_=>close()} className="px-4 bg-primary">Close</Button>
        </div>
    </React.Fragment>   
}


function ExpancesStatusList() {
    const { t } = useTranslation()
    const router = useRouter();
    const trip_id = router.query.trip_id
    const [activeBtn,setActiveBtn] = useState(1)
    const [openPaymentMadeModal ,setOpenPaymentMadeModal] = useState(false)
    const [data,setData] = useState([])
    const [tripDetails,setTripDetails] = useState()
    const [isFullViewExpStatus,setIsFullViewExpStatus] = useState()

    let GetAllTransactions = async () => {
        if(trip_id){
            try {
                const list = await get_transactions({
                    type: "expense",
                    trip_id:trip_id// "linkedin_trip_id = ap-south-1:6376c2e2-a84a-4cd2-b014-8ecf0ef46c2f" 
                })
                setData(list)
            } catch (e) {

            }
        }
    }

    const getTripDetailsData = async () =>{
        if(trip_id){
            try{
                const result = await getTripDetails(trip_id)
                if(result){
                    setTripDetails(result)
                }
            }catch(e){

            }
        }
    }

    let handleOpenPaymentMadeModal = (s=false)=>{
        setOpenPaymentMadeModal(s)
    }

    useEffect(()=>{
        GetAllTransactions()
    },[])

    useEffect(async ()=>{
        getTripDetailsData()
    },[trip_id])

    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }

    const RenderData = ({status})=>{
        if(data){
            return (data || []).map((v,i)=>{
                if(v.status===status){
                   return <div className='row mt-3' key={i+v}>
                        <div className='col'>
                            <div className="justify-content-start align-items-center" style={{display:'-webkit-inline-box'}}>
                                 <Avatar style={{ background: green[500] ,marginRight:10 }} variant="circle">{String(v.reason).split(' ').map(word => word.charAt(0).toUpperCase())}</Avatar>
                                {v.reason}<br />
                                <small className="text-secondary">{getDate(v.date)}</small>
                            </div>

                        </div>
                        <div className='col-5' style={{textAlign:'right'}}>
                            ₹ {Math.abs(v.amount)}
                        </div>
                        <div className='col-1' style={{marginLeft : 10}}
                                onClick={_=>{
                                    setIsFullViewExpStatus(v)
                                    console.log('v = ',v)
                                }}
                            > 
                             <ChevronRight style={{marginLeft : 10}}/>
                             </div>

                </div>
                }else return null
                
            })
        }
        return null
    }

    return (
        isFullViewExpStatus ? <FullViewExpStatus data={isFullViewExpStatus} close={_=>setIsFullViewExpStatus()} />
        :
        <>
        <div className="px-4 py-4" style={{background:'white'}}>
           <div className="row">
               <div className='col'>
                    <ButtonGroup variant="outlined" aria-label="outlined button group">
                            <Button onClick={e=>setActiveBtn(1)} {...(activeBtn===1 ? {variant:"contained",color:"primary"} : {})}>{t('Pending')}</Button>
                            <Button onClick={e=>setActiveBtn(2)} {...(activeBtn===2 ? {variant:"contained",color:"primary"} : {})}>{t('Aproved')}</Button>
                            <Button onClick={e=>setActiveBtn(3)} {...(activeBtn===3 ? {variant:"contained",color:"primary"} : {})}>{t('Reject')}</Button>
                        </ButtonGroup>
                </div>
                <div className="col-3" style={{textAlign:'right'}}>
                    <Button variant="contained" className="bg-primary text-light" onClick={_=>handleOpenPaymentMadeModal(true)}>
                        <svg className="mx-2" width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="9.5" cy="9.5" r="9.5" fill="white"/>
                            <line x1="9.26855" y1="4.38281" x2="9.26855" y2="14.6136" stroke="#5E95F7"/>
                            <line x1="14.6147" y1="9.26562" x2="4.38398" y2="9.26562" stroke="#5E95F7"/>
                        </svg>
                        {t('Add Expense')}
                    </Button>
                </div>

           </div>

        </div>

        <div className="px-4 pt-4" style={{background:'white'}}>
                {
                    activeBtn===1 && <RenderData status="not_settled" />
                }
                {
                     activeBtn===2 && <RenderData status="approved" />
                }
                {
                     activeBtn===3 && <RenderData status="rejected" />
                }
                

        </div>

        {(openPaymentMadeModal && tripDetails) && 
            <AddPaymentMadeModal 
                UpdateTripDetails={GetAllTransactions}
                tripDetails={{
                    ...tripDetails,
                     trip_id: trip_id
                    }}
                ClosePaymentMadeModal={_=>{
                    handleOpenPaymentMadeModal(false)
                }}
            />}

        </>
    )
}

export default ExpancesStatusList