import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useTranslation } from "react-i18next";
import { Box , Button, Fab, TextField, Typography , Modal , Accordion , AccordionDetails , AccordionSummary} from '@material-ui/core'
import { Delete, LocalShippingOutlined } from '@material-ui/icons'
import tripStyles from './../../../styles/TripsData.module.scss'
import INRIcon from './../../../Components/Home/svg/InrIcon.svg'
import { getAllTrips , UpsertDriverAdvance } from '../../../Services/driverKhata'
import moment from 'moment'
import { blue, green, grey } from '@material-ui/core/colors'
import { DatePicker } from '@material-ui/pickers'
import Link from 'next/link';

import { ExpandMore } from '@material-ui/icons';



const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    background: 'white',
    boxShadow: 24,
    padding: 15,
  };




const DeleteModal = ({ isOpen , onClick })=>{  
    return (
      <div>
        <Modal
          open={isOpen}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box style={style}>
            <Typography id="modal-modal-title" variant="h6" component="h2">
               Are you sure, Do you want to Delete this transaction
            </Typography>
            <div className="mt-2 text-left d-flex justify-content-end">
                <div className="p-2"><Button onClick={_=>onClick(true)} className="text-light bg-danger mr-2">Yes</Button></div>
                <div className="p-2"><Button onClick={_=>onClick(false)} className="text-light bg-primary ml-2">No</Button></div>
            </div>
          </Box>
        </Modal>
      </div>
    );
}


function driverTripList() {
    const { t } = useTranslation()
    const router = useRouter();
    const trip_id = router.query.id //this is a driver id
    const [tripData, setTripData] = useState()

    const [isDelete,setIsDelete] = useState({
        isOpen : false,
        onClick : _=>{}
    })

    const [accordation,setAccordation] = useState('')

    const [paymentDetails,setPaymentDetails] = useState({
        trip_id : trip_id,
        amount : null,
        reason : '',
        date : null
    })

    const [TRIP_ID,setTRIP_ID] = useState('')

    let addPaymentHandler = (payload)=>{ 
        setPaymentDetails({...paymentDetails,...payload})
    }

   
    let AllTrips = async () => {
        if(trip_id){
            try {
                const list = await getAllTrips({driver_uid: trip_id})
                    // if(list){
                    //     setTripData(list)
                    // }else{
                    // }
                    setTripData(list)
            } catch (e) {
                console.log('e = ', e)
            }
        }
    }

    let getDate = (milliseconds) => {
        return moment(new Date(milliseconds * 1000)).format('DD-MM-YYYY')
    }


    let handleAddPayment = async ()=>{
        let payloads = {
            trip_id : TRIP_ID,
            amount : Number(paymentDetails.amount),
            reason : paymentDetails.reason || "Payment-1",
            date : (new Date(moment(paymentDetails.date).format("YYYY-MM-DD")).getTime()) / 1000
        }
        
        try{
            const result = await UpsertDriverAdvance(payloads)
            setTimeout(() => {
                AllTrips()
                setPaymentDetails({
                    trip_id : trip_id,
                    amount : null,
                    reason : '',
                    date : null
                })
            }, 1200);
        }catch(e){
           
        }

    }

    let handleDeletePayment = async ({ id , amount, date,reason})=>{
        let payloads = {
            trip_id :TRIP_ID,
            amount : 0,
            id : id,
            reason : 'delete',
            date : (new Date(moment(Date.now()).format("YYYY-MM-DD")).getTime()) / 1000     
        }
        
        try{
            const result = await UpsertDriverAdvance(payloads)
            setTimeout(() => {
                AllTrips()
            }, 600);
           
        }catch(e){
          
        }
    }

    useEffect(()=>{
        AllTrips()
    },[])


    useEffect(()=>{
        if(tripData){
            setTRIP_ID(tripData.trips[0]?.trip_id)
        }
    },[tripData])

    return (
        <>
        { isDelete.isOpen && <DeleteModal isOpen={isDelete.isOpen} onClick={isDelete.onClick} /> }
            <table className={`w-100 rounded-3 position-relative mt-4 ${tripStyles['table']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
                <thead>
                    <tr>
                        <th style={{ width: "1%" }}></th>
                        <th>Part Name</th>
                        <th>Origin</th>
                        <th>Destination</th>
                        <th>Truck Number</th>
                        <th>Status</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                    
                    {(tripData) &&
                        (tripData.trips || []).map(({
                            trip_id,customer_name, destination_city, driver_balance,
                            origin_city, status, trip_start_date,  driver_phone,
                            truck_id , driver_advance
                        }, i) => {
                            let total = '' , advances = []
                            if(driver_advance && 'object'===typeof driver_advance && !Array.isArray(driver_advance)){
                                total = driver_advance.total
                                advances = driver_advance.advances
                            }
                            const numType = n=>-1 >= n ? 'neg' : 'pos'
                            let blType = numType(driver_balance)
                            let acId = String(truck_id+trip_id)
                            // style={{display: acId===accordation ? 'block' :'block'}}
                        return <React.Fragment key={i + trip_id}>
                                <tr style={{ cursor: "pointer" }}>
                                    <td style={{ width: "1%" }}><Fab className={tripStyles[status]} ><LocalShippingOutlined className={tripStyles[status]} /></Fab></td>
                                    <td>
                                        {customer_name}<br />
                                        {driver_phone}
                                    </td>
                                    <td>
                                        {origin_city}<br />
                                        {getDate(trip_start_date)}
                                    </td>
                                    <td>{destination_city}</td>
                                    <td>{truck_id}</td>
                                    <td style={{ textTransform: 'capitalize' }}><span className={tripStyles[status]}>{
                                        status.replace('_', ' ')
                                    }</span></td>
                                    <td>
                                        <div><small>{blType==='neg' ? 'To Pay' : 'To Receive'}</small></div>
                                        <span style={{fontWeight:'bold',color:blType==='neg' ? '#F53636' : '#2dbc53'}}>
                                            <INRIcon className="inr-icon" /> {Math.abs(driver_balance)}
                                        </span>
                                    </td>
                                    <td>
                                        <div onClick={_=>{
                                            setAccordation(acId===accordation ? '' : acId)
                                        }}>
                                            <ExpandMore {...(acId===accordation ? { style : { transform : "rotate(180deg)" } } : {})} />
                                        </div>
                                    </td>
                                </tr>
                                <tr >
                                    <td colSpan={7}>
                                        <div style={{display: acId===accordation ? 'block' :'none'}}>
                                            <div className="row mt-4">
                                                <div className="col" style={{ textAlign: 'left', fontSize: '.9rem' }}>Payment Details</div>
                                                <div className="col-4" onClick={_=>addPaymentHandler({trip_id : trip_id})} style={{ textAlign: 'right', color: '#312968', cursor: 'pointer' }}>+ Add payment </div>

                                                <div className="container px-4" style={{display : ((paymentDetails.trip_id===trip_id) || Array.isArray(advances) && advances.length)? 'block' : 'none'}} >
                                                    {(Array.isArray(advances) && advances.length) && advances.map(({ id , amount, date,reason },i)=>
                                                        <div className="row rounded-3 p-1 mt-3" style={{background:blue[50]}} key={i+id}>
                                                            <div className='col align-self-center' style={{textAlign:'left'}}>
                                                                {reason}<br/>
                                                                <small className='text-default'>Date: {getDate(date)}</small>
                                                            </div>
                                                            <div className='col-4 align-self-center' style={{textAlign:'right'}}>
                                                                <INRIcon className="inr-icon" /> {Math.abs(amount)}
                                                                <span style={{cursor:'pointer'}} onClick={_=>{
                                                                    setIsDelete({
                                                                        isOpen : true,
                                                                        onClick(t){
                                                                            if(t){
                                                                                handleDeletePayment({ id , amount, date,reason})
                                                                            }
                                                                            setIsDelete({
                                                                                isOpen : false,
                                                                                onClick:_=>{}
                                                                            })
                                                                        }
                                                                    })
                                                                }} className='mx-3'><Delete htmlColor={grey[500]} fontSize="small" /></span>
                                                            </div>
                                                        </div>
                                                    )

                                                    }
                                                    { paymentDetails.trip_id===trip_id && 
                                                        <>
                                                            {
                                                            
                                                                <div className='row my-3 border px-3 py-2'>
                                                                    <div className="col">
                                                                        <TextField value={paymentDetails.amount} onChange={e=>addPaymentHandler({amount:e.target.value})} label={t('Enter Amount')} style={{ minWidth: '100%' }} />
                                                                    </div>
                                                                    <div className="col">
                                                                        { addPaymentHandler({ reason : "Payment-"(+advances.length+1) }) }
                                                                        <TextField value={paymentDetails.reason || "Payment-"+(advances.length+1)} onChange={e=>addPaymentHandler({reason:e.target.value || "Payment-"+(advances.length+1)})} label={t('Reason')} style={{ minWidth: '100%' }} />
                                                                    </div>
                                                                    <div className="col">
                                                                        <DatePicker
                                                                            label={t('dd-mm-yyyy')}
                                                                            value={ paymentDetails.date || null }
                                                                            onChange={e=>addPaymentHandler({date : e})}
                                                                            format="DD-MM-YYYY"
                                                                            autoOk
                                                                        />
                                                                    </div>
                                                                    <div className="col-5 align-self-center" style={{ textAlign: 'right' }}>
                                                                        <span style={{cursor:'pointer'}} onClick={_=>{
                                                                            addPaymentHandler({
                                                                                trip_id : '',
                                                                                date : null,
                                                                                amount : null,
                                                                                reason : ''
                                                                            })
                                                                        }}><Button variant="contained" className='bg-default mx-3' size="small">Cancle</Button></span>
                                                                        <span style={{cursor:'pointer'}} onClick={_=>{
                                                                            handleAddPayment()
                                                                        }}><Button variant="contained" className='bg-primay mx-3' size="small">Save</Button></span>
                                                                    </div>
                                                                </div>
                                                        
                                                            }
                                                
                                                        </>
                                                    }
                                                </div>

                                                <div className="w-100 mt-3"></div>
                                                <div className="col" style={{ textAlign: 'left' }}>Total Payments </div>
                                                <div className="col-4 font-weight-bold mx-5" style={{ textAlign: 'right' , fontWeight:'bolder'}}><INRIcon className="inr-icon" /> {total}</div>
                                            </div>
                                            <div className='row pt-5'>
                                                <div className="col"></div>
                                                <div className="col-6" style={{ textAlign: 'right' }}>
                                                        <Button href={"/settings/driverKhata/expancesStatusList?trip_id="+TRIP_ID} variant="outlined" style={{cursor:'pointer' ,width: 'fit-content', background: blue[400] }}  className="border-0 text-light" size="large">Expenses List</Button>
                                                        <Button disabled={tripData?.trips[0]?.status==="settled"} href={"/settings/driverKhata/settleDriver?driver_uid="+router.query.id} variant="outlined" style={{cursor:'pointer' ,width: 'fit-content', background: green[tripData?.trips[0]?.status==="settled" ? 100 : 400] }} className="border-0 text-light mx-3" size="large">
                                                            Settlement
                                                        </Button>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </React.Fragment>
                        })

                    }
                </tbody>
            </table>

            <div className="pt-5 text-center">
                <Link href="/settings/driverKhata/driverList">
                    <Button className="bg-primary px-5 text-light" size="large">CANCLE</Button>
                </Link>
            </div>
        </>
    )
}

export default driverTripList