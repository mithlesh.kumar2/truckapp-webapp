import { useState, useContext, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";
import { PulseLoader } from 'react-spinners'
import { useRouter } from 'next/router'

import { TextField, MenuItem, Button, Select} from '@material-ui/core'
import { DatePicker } from '@material-ui/pickers'
import tripStyles from '../../styles/TripsData.module.scss'
import { green, red, blue } from '@material-ui/core/colors';
import INRIcon from '../../Components/Home/svg/InrIcon.svg'
import Eye from '../../Components/Home/svg/Eye.svg'
import Activateupdown from '../../Components/Home/svg/Activateupdown.svg'
import { GlobalLoadingContext } from '../../Context/GlobalLoadingContext'
import { getTripPOD } from '../../Services/PodServices'
import moment from 'moment'



const podTracker = () => {
    const { t } = useTranslation()
    const router = useRouter();
    const [loading, setLoading] = useState(false);
    const { setGlobalLoading } = useContext(GlobalLoadingContext)
    const [fromDate,setFromDate] = useState(null)
    const [toDate,setToDate] = useState(null)
    const tableDiv = useRef()
    const [Trips, setTrips] = useState({
        from: null,
        message: '',
        lastTrip : false,
        trips: []
    })
    const [inputes, setInputes] = useState({
        search: '',
        status: 'not_settled',
        fromDate: null,
        toDate: null,
        buttonDate: 0,
        sort: {
            on: 'pod_date',
            ascending: false
        },
        from : null
    })

    let handleChange = (v = {}) => {
        setInputes({ ...inputes, ...v })
    }

    let getDate = (ms) =>{
      if(!ms){
          return null
      }
      return moment(new Date(ms * 1000)).format('DD-MM-YYYY')
    }

    let GetPOD = async (from=0,firstLoad) => {
        if(from){
           inputes['from'] = from
        }
        if(firstLoad)setGlobalLoading(true)
        else setLoading(true);

            try {
                const data = await getTripPOD(inputes)
                if(firstLoad)setGlobalLoading(false)
                else setLoading(false);

                if (data) {
                    if(firstLoad || from){
                        setTrips({
                            from : data.from,
                            message : data.message,
                            lastTrip:Boolean(data.trips.length),
                            trips : [...Trips.trips,...data.trips],
                        })
                    }else{
                        setTrips({...data,lastTrip:Boolean(data.trips.length)})
                    }
                }
            } catch (e) {
                if(firstLoad)setGlobalLoading(false)
                else setLoading(false);
            }
    }

    
   let LoadMoreTrips = async () => {
        GetPOD(
           (Trips.from || 0) + 20
        )
    }

    let LoadMoreOnScroll = ()=>{
        if ((tableDiv.current.offsetHeight  + tableDiv.current.scrollTop ) >= tableDiv.current.scrollHeight){
            if(!loading){
                if(Trips.lastTrip){
                    LoadMoreTrips()
                }else tableDiv?.current?.removeEventListener('scroll', LoadMoreOnScroll);
            }
        }
    }

    useEffect(()=>{
        if(toDate && fromDate){
            handleChange({
                buttonDate : 0,
                toDate : toDate,
                fromDate : fromDate,
                from: null
            }) 
        }
    },[fromDate,toDate])

    useEffect(() => {
        GetPOD(0,1)
        tableDiv?.current?.addEventListener('scroll', LoadMoreOnScroll)

    }, [])

    useEffect(() => {
        GetPOD()
        
    }, [inputes])
    const colorVar = inputes.status === 'not_settled' ? red : inputes.status === 'settled' ? green : blue
    return (
        <>

        <div className={`w-100 px-5 mt-4 d-flex justify-content-between align-items-center flex-lg-row flex-md-row flex-column`}>

                    <TextField value={inputes.search} label={t('Search Customer')} onChange={e => handleChange({ search: e.target.value })} style={{ minWidth: '30%' }} />

                    <div className="px-3">
                    <Select
                        className="px-1 ml-4"
                        labelId="select-filter-label"
                        id="demo-simple-select-standard"
                        value={inputes.status || 'all'}
                        onChange={e => handleChange({ status: e.target.value, from:null })}
                        label={t("Filters")}
                    >
                        <MenuItem value="all">{t('All Trips')}</MenuItem>
                        <MenuItem value="settled">{t('Settled Trips')}</MenuItem>
                        <MenuItem value="not_settled">{t('Pending Settlements')}</MenuItem>
                    </Select>
                    </div>
                   
                    <div className="d-flex align-items-center justify-content-evenly flex-wrap">

                        <DatePicker
                            label={t('From date')}
                            value={fromDate}
                            onChange={(d) => setFromDate(d)}
                            format="DD-MM-YYYY"
                            autoOk
                        />

                        <DatePicker
                            label={t('To date')}
                            value={toDate}
                            onChange={(d) => setToDate(d)}
                            format="DD-MM-YYYY"
                            autoOk
                        />
                    </div>
                    <div className="d-flex align-items-center justify-content-evenly flex-wrap">
                        <Button className="my-1" onClick={_ => handleChange({ buttonDate: inputes.buttonDate === 7 ? 0 : 7,  from: null, toDate: null, fromDate: null })}  variant={inputes.buttonDate === 7 ? 'contained' : 'outlined'} color={inputes.buttonDate === 7 ? "primary" : "default"}>7 Days</Button>
                        <Button className="my-1" onClick={_ => handleChange({ buttonDate: inputes.buttonDate === 15 ? 0 : 15, from: null, toDate: null, fromDate: null })} variant={inputes.buttonDate === 15 ? 'contained' : 'outlined'} color={inputes.buttonDate === 15 ? "primary" : "default"}>15 Days</Button>
                        <Button className="my-1" onClick={_ => handleChange({ buttonDate: inputes.buttonDate === 30 ? 0 : 30, from: null,toDate: null, fromDate: null })} variant={inputes.buttonDate === 30 ? 'contained' : 'outlined'} color={inputes.buttonDate === 30 ? "primary" : "default"}>30 Days</Button>
                    </div>

        </div>


            <div>
                <div style={{ color: colorVar[500], backgroundColor: colorVar[50] }} className="d-flex align-items-center justify-content-evenly flex-wrap py-1 mt-3">
                    {inputes.status === 'not_settled' ? t('Pending Settlements') : inputes.status === 'settled' ? t('Settled Trips') : t('All Trips')}
                </div>
                <div style={{maxHeight:400,overflow:'auto'}} ref={tableDiv}>
                    <table className={`w-100 rounded-3 position-relative ${tripStyles['POD']} px-lg-2 px-md-2 px-1 mx-auto`} id="table">
                    <thead className="sticky-top">
                        <tr>
                            <th>Customer</th>
                            <th onClick={_ => { handleChange({ sort: { on: 'pod_date', ascending: inputes.sort.click == 1 ? true : !inputes.sort.ascending, click: 0 } }) }} style={{ transform: inputes.sort.on === 'pod_date' && inputes.sort.ascending ? 'scale(1.1)' : 'scale(1.0)' }}>POD Date<Activateupdown /></th>
                            <th>Number</th>
                            <th onClick={_ => { handleChange({ sort: { on: 'balance', ascending: inputes.sort.click == 0 ? true : !inputes.sort.ascending, click: 1 } }) }} style={{ transform: inputes.sort.on === 'balance' && inputes.sort.ascending ? 'scale(1.1)' : 'scale(1.0)' }}>Balance<Activateupdown /></th>
                            <th>View POD</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            Trips?.trips.map((t) =>
                                <tr key={t.trip_id}>
                                    <td>{t.customer_name}</td>
                                    <td>{getDate(t.pod_submit_date)}</td>
                                    <td>{t.customer_phone}</td>
                                    <td><INRIcon className="inr-icon" />{t.to_receive}</td>
                                    <td><Eye onClick={_ =>{router.push({ pathname: '/settings/podDetails', query: { trip_id: t.trip_id } })}} /></td>
                                </tr>)
                        }
                    
                    </tbody>
                    <tfoot >
                        <tr>
                            <td colSpan="5" className="text-center">
                                {loading ? <PulseLoader size={15} margin={2} color="#36D7B7" /> : null}
                            </td>
                        </tr>
                    </tfoot >
                </table>
                </div>
                
            </div>

        </>

    );
}

export default podTracker;