const path = require('path')

module.exports = ({
  webpack(config,{dev}) {
    if(dev){
      config.devtool = 'cheap-module-source-map'
    }
    config.module.rules.push({
      test: /\.svg$/,
      // issuer: {
      //   test: /\.(js|ts)x?$/,
      // },
      use: ['@svgr/webpack']
    });
    return config;
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  images: {
    domains: ['data-truckapp.s3.amazonaws.com']
  }
});
